---
weight: 22
title: "Conceptos básicos"
description: "Se presentan algunos conceptos básicos"
---


En este momento estamos en la posición de explorar cómo podremos experimentar 
con nuestra abstracción de máquina de cómputo. Tenemos que definir una forma 
sistemática para poder presentar todas las entradas posibles que podría aceptar 
nuestra máquina de cómputo. Para lograr esto vamos a definir unos cuantos 
conceptos basados en teoría de conjuntos.

### Alfabeto

Un alfabeto es un <mark>conjunto **finito** de símbolos básicos</mark>. Habitualmente 
vamos a usar la notación {{< katex >}}\Sigma{{< /katex >}} para referirnos a 
ellos

#### Ejemplos

Algunos ejemplos de alfabetos son: 

1. {{< katex >}}\{a,b\}{{< /katex >}} <a id="ej_sigma1"></a>
2. {{< katex >}}\{0,1\}{{< /katex >}} <a id="ej_sigma2"></a>
3. {{< katex >}}\{import,print,for,in,v1,v2,1,2,3,[,],',',m,'\ '\}{{< /katex >}}
   <a id="ej_sigma3"></a>

El primero es un alfabeto de dos símbolos _a_ y _b_; el segundo se parece pero
los símbolos son _0_ y _1_; finalmente, el último es un alfabeto con 14 símbolos
básicos, es interesante que algunos de ellos no son tan básicos, como _import_ o
_print_, pero a consideración del diseñador de estos alfabetos fueron
suficientemente "básicos" y como podrán notar corresponden a palabras reservadas
del lenguaje de programación [python](https://www.python.org/).

### Cadenas

<mark>Una cadena es una secuencia **finita** de elementos</mark> de un <mark>alfabeto</mark> {{< katex >}}\Sigma{{< /katex >}}. Habitualmente vamos usar la notación {{< katex >}}w{{< /katex >}} para referirnos a una cadena. 

#### Ejemplos

Algunos ejemplos de cadenas son: 

1. {{< katex >}}bbbbaaa,bab,bbabababbbab{{< /katex >}}
2. {{< katex >}}0,1,000,110,11100,001,00000{{< /katex >}}
3. {{< katex >}}import\ m, for\ v1\ in\ [1,2,3]{{< /katex >}}

El primer inciso ejemplifica tres cadenas usando el [primer]({{< relref
"#ej_sigma1">}}) alfabeto, el segundo inciso muestra siete cadenas del [segundo]({{< relref "#ej_sigma2" >}}) y el tercero dos para nuestro alfabeto inspirado en [python]({{< relref "#ej_sigma3" >}}).
#### Longitud de la cadena

De las cadenas podemos saber la cantidad de elementos básicos que la componen:

{{< katex display >}} |w| \rightarrow n {{< /katex >}}

Algunos ejemplos de longitud son:

1. {{< katex >}}|bbbbaaa|=7{{< /katex >}}
2. {{< katex >}}|bab|=3{{< /katex >}}
3. {{< katex >}}|bbabababbbbab|=15{{< /katex >}}
4. {{< katex >}}|0|=1{{< /katex >}}
5. {{< katex >}}|1|=1{{< /katex >}}
6. {{< katex >}}|11100|=5{{< /katex >}}
7. {{< katex >}}|import\ m|=??{{< /katex >}}

{{< details title="Respuesta..."  >}}
3, _import_ es un símbolo básico, _m_ otro y el espacio ' ' el tercer.
{{< /details >}}

#### La cadena vacía 

La cadena dónde se elije no elegir ningún elemento del alfabeto se le conoce 
como la cadena vacía, se le denota con el símbolo {{< katex >}}\varepsilon{{< 
/katex >}} (<mark>epsilón</mark>). Notar que su longitud es cero:

{{< katex display >}}|\varepsilon| = 0 {{< /katex >}}

Algunos notas o libros de texto usan el símbolo {{< katex >}}\lambda{{< /katex >}} (lambda) en lugar del epsilón.
#### Concatenación de dos cadenas

La concatenación es una operación que <mark>toma dos argumentos de tipo cadena y construye una nueva cadena donde se ponen los símbolos de la primera cadena seguidos por los de la segunda cadena</mark>. Si tenemos dos cadenas, {{< katex >}} w_1 {{< /katex>}} y {{< katex >}} w_2 {{< /katex>}}, con símbolos representados por _aes_ (que siguen la notación {{< katex >}} a_{i,j}{{< /katex>}} donde la _i_ representa la cadena y _j_ la posición del símbolo en la cadena) {{< katex >}} w_1=a_{1,1}a_{1,2}\ldots a_{1,n}; w_2=a_{2,1}a_{2,1}\ldots a_{2,m}; {{< /katex>}} entonces la concatenación de {{< katex >}} w_1 {{< /katex>}} y {{< katex >}} w_2 {{< /katex>}} es:

{{< katex display>}}w_c=a_{1,1}a_{1,2}\ldots a_{1,n}a_{2,1}a_{2,1}\ldots a_{2,m}{{< /katex>}}.

Algunos ejemplos con cadenas específicas:

1. {{< katex >}}b{{< /katex >}} y {{< katex >}}a{{< /katex >}} {{< katex 
>}}=ba{{< /katex >}}
1. {{< katex >}}a{{< /katex >}} y {{< katex >}}b{{< /katex >}} {{< katex 
>}}=ab{{< /katex >}}
1. {{< katex >}}ab{{< /katex >}} y {{< katex >}}aa{{< /katex >}} {{< katex 
>}}=abaa{{< /katex >}}
1. {{< katex >}}0{{< /katex >}} y {{< katex >}}1{{< /katex >}} {{< katex 
>}}=01{{< /katex >}}
1. {{< katex >}}\varepsilon{{< /katex >}} y {{< katex >}}1{{< /katex >}} {{< 
katex >}}=1{{< /katex >}}
1. {{< katex >}}0{{< /katex >}} y {{< katex >}}\varepsilon{{< /katex >}} {{< 
katex >}}=0{{< /katex >}}
1. {{< katex >}}001{{< /katex >}} y {{< katex >}}\varepsilon{{< /katex >}} {{< 
katex >}}=??{{< /katex >}}

{{< details title="Respuesta..."  >}}
001
{{< /details >}}


##### Propiedades de la concatenación

La concatenación entre dos cadenas tiene las siguientes propiedades

1. {{< katex >}}w_1w_2 \neq w_2w_1{{< /katex >}} (__no conmutativa__)
1. {{< katex >}}w_1w_2w_3 = (w_1w_2)w_3 = w_1(w_2w_3){{< /katex >}}
   (__asociativa__)
1. {{< katex >}}w\varepsilon = \varepsilon w{{< /katex >}} (__elemento neutro__)
1. {{< katex >}}|w_1w_2| = |w_1|+|w_2| {{< /katex >}} (__suma de longitudes__)

### Lenguajes

<mark>Conjunto de cadenas</mark> basado en un alfabeto {{< katex >}}\Sigma{{< /katex >}}. 
Habitualmente vamos a usar la notación {{< katex >}}L{{< /katex >}} para 
referirnos a ellos.

#### Ejemplos

Algunos ejemplos de lenguajes son: 

1. {{< katex >}}\{a,aa,aaa,aaaa,aaaaa,aaaaaa\}{{< /katex >}}
1. {{< katex >}}\{ba,aaab,aaba,aabb,aaaaaabbb\}{{< /katex >}}
1. {{< katex >}}\{a,b,aa,ab,ba,bb,aaa,aba,\ldots\}{{< /katex >}}
1. {{< katex >}}\{a,b\}{{< /katex >}}

Ojo, ¿por qué es interesante el tercer lenguaje? ¿y, el cuarto?

{{< details title="Respuesta..."  >}}
El tercer lenguaje es __infinito__, dado que solo se señala que un lenguaje es
un conjunto se permite que estos sean infinitos. El cuarto lenguaje se podría
confundir con alfabeto tal como luce; lo único que lo hace lenguaje es el hecho
que aparece en una lista de lenguajes.
{{< /details >}}

#### Lenguajes notables

##### El lenguaje de la cadena vacía

Es el lenguaje que <mark>contiene únicamente a la cadena vacía</mark>

{{< katex >}}L_{\varepsilon}=\{\varepsilon\}{{< /katex >}}

Hay que notar que 
{{< katex >}}|L_{\varepsilon}|=1{{< /katex >}} aún cuando la longitud de ese 
elemento sea cero (i.e., {{< katex >}}|\varepsilon|=0{{< /katex >}})

##### El lenguaje vacío

Es el lenguaje que <mark>no contiene ninguna cadena</mark>

* {{< katex >}}L_{\emptyset}=\{\ \}{{< /katex >}}
* {{< katex >}}|L_{\emptyset}|=0{{< /katex >}} 


### Potencia de un alfabeto

La potencia {{< katex >}}n{{< /katex >}} de un alfabeto ({{< katex >}} 
\Sigma^n{{< /katex >}}) resulta en <mark>un lenguaje conformado por las cadenas que 
se puedan componer concatenando {{< katex >}}n{{< /katex >}} símbolos del 
alfabeto</mark>.

#### Ejemplos de potencias de un alfabeto

Considere el alfabeto {{< katex >}}\Sigma=\{a,b\}{{< /katex >}}

1. {{< katex >}}\Sigma^1 = \{a,b\}{{< /katex >}}
1. {{< katex >}}\Sigma^2 = \{aa,ab,ba,bb\}{{< /katex >}}
1. {{< katex >}}\Sigma^3 = \{aaa,baa,aba,aab,abb,bab,bba,bbb\}{{< /katex >}}
1. {{< katex >}}\Sigma^4 = \{aaaa,baaa,abaa,aaba,\ldots,bbbb\}{{< /katex >}}

Considere la potencia cero del mismo alfabeto:

1. {{< katex >}}\Sigma^0 = \{\varepsilon\}{{< /katex >}}

Este resultado parecería extraño ¿por qué la potencia cero, genera un lenguaje
con un elemento y no el lenguaje vació? Hay que recordar que la potencia indica
el número símbolos a concatenar en las cadenas de ese lenguaje, y la cadena que
se hace concatenando cero símbolos, es la cadena vacía.

#### Otro lenguaje notable

Si unimos todas las potencias de un alfabeto obtenemos <mark>el lenguaje de todas las 
cadenas posibles de ese alfabeto</mark> para el cual usamos la notación{{< katex >}}\Sigma^*{{<  /katex >}}:

{{< katex display >}}\Sigma^* = \{\varepsilon\} \cup \Sigma^1 \cup \Sigma^2 
\cup \Sigma^3 \cup \Sigma^4 \cup \ldots {{< /katex >}}

Una consecuencia de definir a este lenguaje es que cualquier {{< katex >}}L{{< /katex >}} basado en un {{< katex >}}\Sigma{{< /katex >}} es un subconjunto de {{< katex >}}\Sigma^*{{< /katex >}}. En notación de conjuntos esta circunstancis se escribe como:

{{< katex display >}} L \subset \Sigma^*{{< /katex >}}
