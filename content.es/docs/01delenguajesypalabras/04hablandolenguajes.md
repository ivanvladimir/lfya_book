---
weight: 24
title: "Formas de hablar de los lenguajes"
description: "Enumeramos las formas que conocemos en las que podemos hablar sobre los lenguajes"
---

Hasta ahora hemos visto diferentes formas de hablar sobre los lenguajes:

1. Enumerando los elementos del lenguaje

> {{< katex >}} L=\{a,b,aa,ab,ba,bb,\ldots\} {{< /katex >}}

2. Usando una combinación de operaciones

> {{< katex >}} \{a,b\}^* {{< /katex >}}

3. Describiendo una propiedad

> {{< katex >}} \{ w | w \text{ esta compuesta por los símbolos a y b}\} {{< /katex >}}

4. De forma descriptiva

> El lenguaje cuyas cadenas están compuesta por _aes_ y _bes_

Durante el material aprenderemos nuevas formas de hablar de los lenguajes.
