---
weight: 42
title: "Lema de bombeo para lenguajes regulares"
description: "Se presenta el lema de bombeo para los lenguajes regulares"
---

Una consecuencia de qué podamos particionar en _xyz_ a una cadena _w_ perteneciente a un
Lenguaje Regular con una longitud mayor que el número de estados del AF que la
acepta, es que ese ciclo asociado al infijo, lo podríamos repetir múltiples
veces y la cadena resultante también debería pertenecer al mismo lenguaje. Por ejemplo:

Considerando el ejemplo de la sección anterior; si _w_ es _abbbbbb_, donde _x_
es _a_, _y_ es _bbb_ y _z_ es _bbb_; entonces si repetimos varias veces _y_
obtendríamos las nuevas cadenas:

* Con una repetición, _a bbbbbb bbb_ pertenece al mismo lenguaje (espaciado usado para señalar donde comienza la parte
    que se repite)
* Con dos repeticiones, _a bbbbbbbbb bbb_ pertenece al mismo lenguaje
* etc

A esta consecuencia de la existencia de la partición se le conoce como el _lema
de bombeo_.

### Lema de bombeo

Sea _L_ un Lenguaje Regular y su AF con _n_ estados que lo acepta, entonces
para todas la cadenas _w_ donde {{< katex >}} |w| \geq n {{< /katex >}} se
podrán descomponer en tres partes _xyz_ tales que:

1. {{< katex >}} |xy| \leq n {{< /katex >}}
2. {{< katex >}} y \ne \varepsilon {{< /katex >}}, puesto de otra forma {{< katex >}} y > 1 {{< /katex >}}
3. Para todo {{< katex >}} k \geq 0 {{< /katex >}}, las cadenas con la forma {{< katex >}} xy^kz {{< /katex >}} también pertenecen a _L_


### Aplicación del lema de bombeo

Algo que se podría hacer con el lema de bombeo es usarlo para demostrar que un
lenguaje es regular de la siguiente forma:

1. Identificar un lenguaje regular _L_
2. Escoger una longitud _n_ para _xy_
3. Proponer una forma de cadena que dependa de _n_ pero que sea mayor que _n_
4. Particionar la cadena _w_ en _xyz_
5. Verificar que la partición cumpla con las restricciones:
   1. {{< katex >}} xyz = w {{< /katex >}}
   1. {{< katex >}} |xy| \leq n {{< /katex >}}
   1. {{< katex >}} y \ne \varepsilon {{< /katex >}}
6. Generar cadenas bombeadas {{< katex >}} xy^kz \in L{{< /katex >}} 

Si se cumple para <mark>todas las formas</mark> de la cadena del lenguaje _L_ podríamos
concluir que es un lenguaje regular.

### Cómo realmente usamos el lema de bombeo

Si tratáramos de demostrar que un lenguaje es regular tendríamos que
enumerar *todas las formas* y para todas demostrar que al bombear la cadena
pertenece al lenguaje; esto no va a ser directo porque tendríamos que demostrar
que tenemos una lista de todas las formas posibles de las cadenas del lenguaje,
regresando a nuestro problema original. En realidad no usamos
el lema de bombeo para demostrar que un lenguaje es regular... ¿entonces para
qué lo usamos? <mark>Lo usamos para demostrar que un lenguaje no es
regular</mark>, vamos a
partir de qué es regular y si para una de las formas del lenguaje regular no se
cumple el lema de bombeo, habremos demostrado que un lenguaje no es regular.

### Ejemplo de uso del lema de bombeo

Dado {{< katex >}} \Sigma = \{a,b\} {{< /katex >}} demostrar que el lenguaje
formado por cadenas con la forma {{< katex >}} a^nb^n {{< /katex >}} no es
regular. Para esto, vamos a seguir la aplicación del lemma de bombeo:

1. Identificar un lenguaje regular
   {{< katex >}} L =\{ w | w=a^nb^n\} {{< /katex >}}

2. Escoger una longitud _n_</br>
   Qué tal si en lugar de escoger un valor específico, escogemos cualquier valor, es
   decir _n_

3. Proponer una forma de cadena que dependa de _n_ pero que sea mayor que _n_
   Propongo: {{< katex >}} a^nb^n {{< /katex >}}

4. Particionar la cadena _w_ en _xyz_
   {{< katex >}} x=\varepsilon, y=a^n, z=b^n  {{< /katex >}}

5. Verificar que la partición cumpla con las restricciones:
   1. {{< katex >}} xyz = \varepsilon a^nb^n {{< /katex >}} ✅
   1. {{< katex >}} |xy| = n {{< /katex >}} ✅
   1. {{< katex >}} y \ne \varepsilon {{< /katex >}} ✅

6. Generar cadenas bombeadas {{< katex >}} xy^kz in L {{< /katex >}} 
   1. Para _k=0_, {{< katex >}} xyz = \varepsilon b^n {{< /katex >}} pero {{<
      katex >}}\varepsilon b^n \notin L {{< /katex >}} ❎
   1. Para _k=2_, {{< katex >}} xyz = \varepsilon a^{2n}b^n {{< /katex >}} pero
      {{< katex >}}\varepsilon a^{2n}b^n \notin L {{< /katex >}} ❎


Dado que no se cumplió para cualquier valor de _k_ que la cadena bombeada
perteneciera al lenguaje original, algo hicimos mal. En este caso lo que hicimos
mal fue haber dicho que _L_ era regular. Por lo anterior concluimos que 
el lenguaje formado por cadenas con la forma {{< katex >}} a^nb^n {{< /katex >}}
<mark>no es regular</mark>.

¿El procedimiento pudo haber sido hecho de forma diferente?

{{< details title="Respuesta" open=false >}}
Sí, hay varios puntos de decisión; por ejemplo los siguientes a partir del paso
4.

_Paso 4_

{{< katex >}} x=a^{n-1}, y=a, z=b^n  {{< /katex >}}

_Paso 5_

   {{< katex >}} xyz = a^{n-1}a b^n = a^nb^n {{< /katex >}} ✅</br>
  
   {{< katex >}} |xy| = n {{< /katex >}} ✅</br>
   
   {{< katex >}} y \ne \varepsilon {{< /katex >}} ✅


_Paso 6_

Para _k=0_, 

{{< katex >}} xyz = a^{n-1} b^n {{< /katex >}} pero {{<
      katex >}}a^{n-1} b^n \notin L {{< /katex >}} ❎
   
Para _k=2_,

{{< katex >}} xyz = a^{n-1}a^2b^n = a^{n+1}b^n {{< /katex >}} pero
      {{< katex >}}a^{n+1}b^n \notin L {{< /katex >}} ❎

Por lo tanto también llegamos a la conclusión que no es un lenguaje regular.
{{< /details >}}




### El lenguaje de las cadenas {{< katex >}} a^nb^n {{< /katex >}}

A diferencia de los lenguajes que hasta este momento habíamos visto, este
lenguaje pide algo que no habíamos visto antes. Una parte del lenguaje depende
del otra parte. La parte de _bes_ depende de la de _aes_ ya que el número de
_aes_ define cuantas _bes_ habrá; entonces necesitamos una forma de pasar esta
información a la parte de _bes_, pero hasta ahora no tenemos una forma de pasar
información entre los estados, pero los estados mismos. Con esto en mente, si
tratáramos de diseñar un AF para este lenguaje quedaría de esta forma:

<center>
{{< figure
src="../anbn.svg" title="Un numero de aes seguidas por el mismo número de bes" >}}
</center>

Como es evidente de ese diseño, ese autómata no es finito; por lo tanto no cabe
dentro de nuestra definición de ningún autómata finito, por lo tanto tampoco es
un lenguaje regular.

