---
weight: 45
title: "Árboles de derivación"
description: "Se presentan los árboles de derivación"
---

Un árbol de derivación es una forma de visualizar las relaciones que tienen las
cabezas de reglas junto con los cuerpos de estas. En un árbol de derivación, los
elementos del cuerpo de una regla se convierte en nodos hijos de la cabeza de la
regla.

### Ejemplo árbol de derivación para la cadena _aaabbb_

Para la gramática {{< katex >}} S \rightarrow aSb | \varepsilon  {{< /katex >}}
y la cadena _aaabbb_ el árbol de derivación es:

<center>
{{< figure
src="../arbol_aaabbb.svg" title="Árbol de derivación para la cadena 'aaabbb'" >}}
</center>

Para este ejemplo, sólo tenemos una derivación posible y sólo tenemos un árbol
de derivación.


### Ejemplo árbol de derivación para la cadena _(a<sup>\*</sup>ba<sup>\*</sup>ba<sup>\*</sup>)<sup>\*</sup>_

Para la gramática 
 {{< katex display >}} 
 \begin{array}{rl}
R\rightarrow & B\\
R\rightarrow & R+R\\
R\rightarrow & R^*\\
R\rightarrow & RR\\
R\rightarrow & (R)\\
B\rightarrow & a\\
B\rightarrow & b\\
B\rightarrow & \epsilon\\
B\rightarrow & \emptyset\\
 \end{array}
 {{< /katex >}} 
y las derivaciones de la cadena {{< katex >}}(a^*ba^*ba^*)^* {{< /katex >}}
vistas en la [sección anterior]({{< ref "04ejemplogramática#ejemplos-de-derivaciones" >}}) el árbol de derivación
resulta:

<center>
{{< figure
src="../arbol_re.svg" title="Árbol de derivación para ER de número par de bes" >}}
</center>

En este caso varias derivaciones pueden resultar en un sólo árbol de derivación.
Sin embargo, esta gramática tiene múltiples derivaciones y alguna otras generan
otros árboles de derivación.

<center>
{{< figure
src="../arboles_re.svg" title="Todos los árboles de derivación (28) para ER de número par de bes" >}}
</center>

{{< hint info >}}
**En resumen**

Para una gramática _G_ un cadena puede tener múltiples derivaciones, dependiendo
de la estrategia de sustitución que se elija; algunas de estas derivaciones
resultarán en un solo árbol de derivación; sin embargo, es posible que algunas
derivaciones de la cadena resulten en múltiples árboles de derivación, por lo
tanto una cadena tenga asociadas múltiples árboles de derivación.

{{< /hint >}}
