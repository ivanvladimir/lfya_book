---
weight: 44
title: "Ejemplo de gramática"
description: "Un ejemplo de gramática libre de contexto"
---

La siguiente gramática genera cadenas que representan Expresiones Regulares. 

 {{< katex display >}} 
 \begin{array}{rl}
R\rightarrow & B\\
R\rightarrow & R+R\\
R\rightarrow & R^*\\
R\rightarrow & RR\\
R\rightarrow & (R)\\
B\rightarrow & a\\
B\rightarrow & b\\
B\rightarrow & \epsilon\\
B\rightarrow & \emptyset\\
 \end{array}
 {{< /katex >}} 

### Ejemplos de derivaciones

La siguiente es un ejemplo de una derivación para producir la cadena {{< katex >}}(a^*ba^*ba^*)^*{{< /katex >}},
cabe notar que siempre se cambia el no terminal más a la izquierda. 


{{< katex display >}} 
\begin{array}{rl}
\underline{R} \Rightarrow &  \underline{R}^*\\
  \Rightarrow &  (\underline{R})^*\\
  \Rightarrow &  (\underline{R}R)^*\\
  \Rightarrow &  (\underline{R}RR)^*\\
  \Rightarrow &  (\underline{R}^*RR)^*\\
  \Rightarrow &  (\underline{B}^*RR)^*\\
  \Rightarrow &  (a^*\underline{R}R)^*\\
  \Rightarrow &  (a^*\underline{B}R)^*\\
  \Rightarrow &  (a^*b\underline{R})^*\\
  \Rightarrow &  (a^*b\underline{R}^*)^*\\
  \Rightarrow &  (a^*b\underline{R}R^*)^*\\
  \Rightarrow &  (a^*b\underline{R}RR^*)^*\\
  \Rightarrow &  (a^*b\underline{R}^*RR^*)^*\\
  \Rightarrow &  (a^*b\underline{B}^*RR^*)^*\\
  \Rightarrow &  (a^*ba^*\underline{R}R^*)^*\\
  \Rightarrow &  (a^*ba^*B\underline{R}^*)^*\\
  \Rightarrow &  (a^*ba^*b\underline{R}^*)^*\\
  \Rightarrow &  (a^*ba^*b\underline{B}^*)^*\\
  \Rightarrow &  (a^*ba^*ba^*)^*\\
 \end{array}
{{< /katex >}} 

A la derivación anterior se le conoce como <mark>derivación por la izquierda</mark>. También es posible hacerlo de derecha a izquierda:


{{< katex display >}} 
\begin{array}{rl}
\underline{R} \Rightarrow &  \underline{R}^*\\
  \Rightarrow &  (\underline{R})^*\\
  \Rightarrow &  (R\underline{R})^*\\
  \Rightarrow &  (RR\underline{R})^*\\
  \Rightarrow &  (RR\underline{R}^*)^*\\
  \Rightarrow &  (RR\underline{B}^*)^*\\
  \Rightarrow &  (R\underline{R}a^*)^*\\
  \Rightarrow &  (R\underline{B}a^*)^*\\
  \Rightarrow &  (\underline{R}ba^*)^*\\
  \Rightarrow &  (R\underline{R}ba^*)^*\\
  \Rightarrow &  (R\underline{R}^*ba^*)^*\\
  \Rightarrow &  (R\underline{B}^*ba^*)^*\\
  \Rightarrow &  (\underline{R}a^*ba^*)^*\\
  \Rightarrow &  (R\underline{R}a^*ba^*)^*\\
  \Rightarrow &  (R\underline{B}a^*ba^*)^*\\
  \Rightarrow &  (\underline{R}ba^*ba^*)^*\\
  \Rightarrow &  (\underline{R}^*ba^*ba^*)^*\\
  \Rightarrow &  (\underline{B}^*ba^*ba^*)^*\\
  \Rightarrow &  (a^*ba^*ba^*)^*\\
\end{array}
{{< /katex >}} 

A esta estrategia para derivar se le conoce como <mark>derivación por la derecha</mark>.

Lo anterior nos muestra que una cadena puede tener múltiples derivaciones.
