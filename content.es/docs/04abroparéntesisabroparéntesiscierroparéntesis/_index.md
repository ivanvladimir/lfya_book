---
weight: 4
title: "Abro paréntesis, abro paréntesis, cierro paréntesis"
bookCollapseSection: true
---

{{< section >}}

## Enlaces para explorar


* <i>[What is the Pumping Lemma](https://www.youtube.com/watch?v=qtnNyUlO6vU)</i>. (2020, nov 5) [Video]
* <i>[Nonregular languages: How to use the Pumping Lemma](https://www.youtube.com/watch?v=Ph7Z9YttM0Q)</i>.
    (2020, 5 nov)
* <i>[Lema del bombeo para lenguajes
    regulares](https://es.wikipedia.org/wiki/Lema_del_bombeo_para_lenguajes_regulares)</i>.
    (s/f) [Wikipedia]
* <i>[Unidad 10: Gramáticas Formales](https://www.youtube.com/watch?v=ZBs4Yv90jxc)</i>. (2020, jun 27)
    [Video]
* <i>[Context-Free Grammar Examples - Digital Poetry with Context-Free
    Grammars](https://www.youtube.com/watch?v=R_OVyFrBhiU)</i>. (2018, may 17). [Video]</br>
* <i>[Gramática libre de
    contexto](https://es.wikipedia.org/wiki/Gram%C3%A1tica_libre_de_contexto)</i>. (s/f). [Wikipedia]</br>
* <i>[]()</i>. (s/f). [Wikipedia]</br>
* <i>[Context-free Grammar
    Visualizer](http://mumin.ling.su.se/cfg-visualizer/)</i>. (s/f) [Demo]</br>
* <i>[Coding Challenge #43: Context-Free
    Grammar](https://www.youtube.com/watch?v=8Z9FRiW2Jlc)</i>. (2016, 21 oct)
    [Video]</br>
* <i>[CFG
    Developer](https://web.stanford.edu/class/archive/cs/cs103/cs103.1156/tools/cfg/)</i>. (s/f) [Demo]</br>
 * <i>[Arroz a la zorra](https://radioambulante.org/audio/arroz-a-la-zorra)</i>.
(2022, marzo 01) [Episodio, podcast Radio Ambulante]</br>
 

## Lecturas recomendadas

* Bar-Hillel, Y., Perles, M., & Shamir, E. (1961). On formal properties of
    simple phrase structure grammars. _Sprachtypologie und
    Universalienforschung_, 14, 143-172.
    [[Liga]](https://www.proquest.com/openview/fb41296047fb7453dcb1de182b4aa0b6/1?pq-origsite=gscholar&cbl=1817266)
* Hopcroft, J. E., Motwani, R., Ullman, J. D. Introducción a la teoría de
    autómatas, lenguajes y computación 2a. edición Madrid Pearson Education,
    2002. **Capítulo 4 y 5**
    [[BiDi/UNAM](http://pbidi.unam.mx:8080/login?url=http://search.ebscohost.com/login.aspx?direct=true&db=cat02025a&AN=lib.MX001001976666&lang=es&site=eds-live)]
* Carrasco Jiménez, R. C.; Rubio, J. C.; Zubizarreta, M. L. F. Capítulo 1:
    Lenguajes y computadores. Teoría de Lenguajes, Gramáticas y Autómatas Para
    Informáticos, [s. l.], p. 11–22, 2000. **Capítulo 6 y 7**
    [[BiDi/UNAM](http://pbidi.unam.mx:8080/login?url=https://search.ebscohost.com/login.aspx?direct=true&db=ufh&jid=87PC&lang=es&site=eds-live)]
    [[Liga](https://www.digitaliapublishing.com/a/812/teoria-de-lenguajes--gramaticas-y-automatas-para-informaticos)]
* Giró J, Vazquez J, Meloni B, Constable L. Lenguajes Formales y Teoría de
    Autómatas. Alfaomega. **Capítulo 2**
    [[BiDi/UNAM](http://pbidi.unam.mx:8080/login?url=http://search.ebscohost.com/login.aspx?direct=true&db=cat02025a&AN=lib.MX001001966916&lang=es&site=eds-live)]
    [[Liga](https://libroweb.alfaomega.com.mx/book/lenguajes_formales_teoria_automatas)]
* Hernández Rodríguez LA, Jaramillo Valbuena S, Cardona Torres SA. Practique
    La Teoría de Autómatas y Lenguajes Formales. Ediciones Elizcom; 2010.
    **Capítulo 5**
    [[BiDi/UNAM](http://pbidi.unam.mx:8080/login?url=http://search.ebscohost.com/login.aspx?direct=true&db=cat02025a&AN=lib.MX001001974225&lang=es&site=eds-live)]
    [[Liga](https://www.proquest.com/docview/2135020663/D0AE11F098D846F9PQ/1)]
* Cantú Treviño, T. G., & Mendoza García, M. G. (2015). Teoría de autómatas :
    un enfoque práctico (Primera edición). Pearson Educación de México.
    **Capítulo 6**
    [[BiDi/UNAM](http://pbidi.unam.mx:8080/login?url=https://search.ebscohost.com/login.aspx?direct=true&db=cat02025a&AN=lib.MX001001976665&lang=es&site=eds-live)]
* Brena, R. (2013). Autómatas y lenguajes. McGraw-Hill Interamericana.
    **Capítulo 4**
  [[BiDi/UNAM](http://pbidi.unam.mx:8080/login?url=https://search.ebscohost.com/login.aspx?direct=true&db=cat02025a&AN=lib.MX001001698825&lang=es&site=eds-live)]
* Crespi Reghizzi S, Breveglieri L, Morzenti A. Formal Languages and
    Compilation. Second edition. Springer; 2013. **Sección 2.5**
    [[BiDi/UNAM](http://pbidi.unam.mx:8080/login?url=http://search.ebscohost.com/login.aspx?direct=true&db=cat02025a&AN=lib.MX001001645742&lang=es&site=eds-live)]
    [[Liga](https://www.springer.com/gp/book/9783030048785)]


