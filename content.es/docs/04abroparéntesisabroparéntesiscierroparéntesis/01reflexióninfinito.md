---
weight: 41
title: "Reflexión infinito en LR"
description: "Se reflexiona sobre los lenguajes regulares infinitos en autómatas finitos"
---

Como su nombre lo establece un autómata finito es <mark>finito</mark>, sin
embargo dada la forma de cómo lo definimos estos pueden lidiar con lenguajes
infinitos ¿Cuál es el mecanismo que hace esto posible?

{{< details title="Respuesta" open=false >}}
Es la operación _cerradura_, ni concatenación ni unión general un lenguaje
infinito de un finito; pero la cerradura sí.
{{< /details >}}
Es el hecho de que los AF permiten ciclos, es decir que una transición regrese a un
estado ya visitado abre la oportunidad de aceptar un número infinito de
cadenas. 

Una pregunta que surge es: ¿cuál es la relación entre el número de estados (_n_)
de un AF en particular y el tamaño de una cadena que es aceptada por el
autómata? Un análisis de esta situación, nos lleva a detectar dos casos:

* Si {{< katex >}} |w| < n {{< /katex >}} quiere decir que estamos seguros de que no se han usado
    todos los estados.

* Si {{< katex >}} |w| >=  n {{< /katex >}} quiere decir que estamos seguros que se
    ha repetido uno de los estados. 

En el primer caso no podemos asegurar que no se ha repetido un estado, ya que la
cadena aunque menor que el número de estados puede que atraviese un ciclo; pero
en el segundo caso estamos seguros que al menos un estado ha sido visitado dos veces. 

### Ejemplo con AF

El siguiente autómata tiene cinco estados  acepta cadenas con número de _bes_ divisibles entre tres
o puras aes _aes_, en particular no acepta la cadena vacía.


<center>
{{< figure
src="../tres_bes_o_puras_aes.svg" title="Número par de bes" >}}
</center>

Con esto es mente para cadenas aceptadas podemos llenar la siguiente tabla


| Cadena | Longitud | Se repitió estado | # de ciclos | Posición de primer ciclo | Longitud primer ciclo |
|:------:|:--------:|:-----------------:|:-----------:|:------------------------:|:--------------:|
| _a_          | 1  | No | 0 |  | | 
| _aa_         | 2  | Sí | 1 | 2, _q4_ | 1 |
| _bbb_        | 3  | No | 0 |  | |
| _bbba_       | 4  | Sí | 1 | 4, _q3_ | 1 |
| _abbb_       | 4  | No | 0 |  | |
| _aaaaa_      | 5  | Sí | 4 | 2, _q4_ | 1 |
| _bbbbbb_     | 6  | Sí | 1 | 4, _q1_ | 3 |
| _abbbbbb_    | 7  | Sí | 1 | 5, _q1_ | 3 |
| _ababbbbb_   | 8  | Sí | 1 | 3, _q1_ | 1 |
| _abbabbbb_   | 8  | Sí | 1 | 4, _q2_ | 1 |
| _abbbabbb_   | 8  | Sí | 1 | 5, _q3_ | 1 |
| _bbbbbbbbb_  | 9  | Sí | 2 | 4, _q1_ | 3 |
| _abaabbbbb_  | 8  | Sí | 2 | 3, _q1_ | 1 |
| _abbaabbbb_  | 9  | Sí | 2 | 4, _q2_ | 1 |
| _abbbaabbb_  | 9  | Sí | 2 | 5, _q3_ | 1 |

Como podemos ver una vez que alcanza la longitud mayor o igual que el número de
estados, podemos estar seguros que al menos un estado se está repitiendo.

Por supuesto, suponiendo cualquier AF hay varias preguntas que surgen:

* ¿Si existe el ciclo, dónde está ese ciclo?
  {{< details title="Respuesta" open=false >}}
  Si existe, puede ocurrir
  antes de alcanzar la longitud _n_; no sólo eso, si la cadena es mayor que el
  número de estados podemos estar seguro que el primer ciclo ocurre antes o al alcanzar la longitud
  _n_.
  {{< /details >}}

* ¿Si existe el ciclo, cuántos ciclos hay?
  {{< details title="Respuesta" open=false >}}
  No sabemos con certeza, depende de la estructura del lenguaje, pero si
  estamos seguros que existe al menos uno.
  {{< /details >}}
* ¿Si existe el ciclo, qué longitud tiene?
  {{< details title="Respuesta" open=false >}}
  No sabemos con certeza, depende de la estructura del lenguaje.
  {{< /details >}}

### Dividiendo la cadena en tres

Supongamos que una cadena _w_ pertenece a un lenguaje regular, supongamos que el
AF asociado a este lenguaje tiene _n_ estados y que {{< katex >}} |w| \geq n {{<
/katex >}}; es decir estamos seguros que al menos un estado se repite. Esto
implica que también estamos seguros que el primer ciclo ocurre antes de alcanzar
la longitud _n_. Considerando esto podemos dividir nuestra cadena _w_ en tres partes
_xyz_; a la primera parte la llamamos _prefijo_ (_x_), la segunda _infijo_  (_y_) y la
tercera _sufijo_ (_z_); 
en particular vamos buscar  {{< katex >}} |xy| \leq n {{< /katex >}}
mientras {{< katex >}} |xyz| = |w| {{< /katex >}}. Además vamos a considerar que la
parte _y_  corresponde al ciclo, no sabemos dónde está pero dado que _xyz_ es
mayor que el número de estados, sabemos que un ciclo está antes o cuando
la cadena alcanza la longitud igual al número de estados.

### ¿El número de estados?

El objetivo de que podamos dividir una cadena de un lenguaje regular en tres
partes _xyz_, nos permite explorar un lenguaje que supongamos regular, sin
necesidad de tener que definirlo de las cinco formas que conocemos:

1. Identificar una secuencia de operaciones de lenguajes regulares
2. Diseñar una expresión regular
3. Diseñar un Autómata Finito
3. Diseñar un Autómata Finito No Determinístico
3. Diseñar un Autómata Finito No Determinístico con transición épsilon

Por ejemplo para un lenguaje _L_ que supongo regular e infinito, podré asegurar lo
siguiente:

1. Una cadena _w_ de ese lenguaje _L_ lo suficientemente grande, es decir con {{< katex >}} |w| \geq n {{<
/katex >}}, la podré dividir en tres partes.
2. Donde las dos primeras partes podrían tener estas longitudes {{< katex >}} |xy| \leq n {{< /katex >}}
3. Entonces existirá un ciclo en esas dos primeras partes

Es importante notar que no importa el valor de _n_ (es decir el número de
estados asociados al AF que define el lenguaje), si este es muy elevado,
siempre será posible encontrar una cadena de longitud _n_ o mayor. La labor de
identificar _x_, _y_ y _z_ es nuestra y dependerá de las propiedades del
lenguaje, no sabemos de antemano como son; sólo que existen. Sin embargo la existencia
nos brinda una forma de hablar de los lenguajes regulares, sin hacer el esfuerzo
de un diseño.
