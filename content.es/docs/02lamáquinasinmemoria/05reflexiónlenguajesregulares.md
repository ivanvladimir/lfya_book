---
weight: 25
title: "Reflexión sobre autómatas finitos"
description: "Se revisan algunas propiedades de los autómatas finitos"
---

### ¿Qué sabe un AF?

Para procesar una cadena lo único que sabe un autómata es:

1. La posición de la cadena que ya analizó
2. El estado en el que se encuentra

#### ¿Qué le pasa al espacio de {{< katex >}}\Sigma^*{{< /katex >}} mientras procesamos una cadena?

Nos movemos entre los conjuntos de cadenas aceptadas y cadenas rechazadas. El
espacio lo podemos dividir en dos partes: cuando estamos en una estado no final y cuando estamos en un estado final.

Por ejemplo, para el lenguaje de cadenas con número par de _bes_ 
([aquí como expresión regular]({{< ref "02leguajesregulares#ejemplo-número-de-_bes_-pares" >}}) y
[aquí como autómata finito]({{< ref "04autómatafinito/#ejemplo-número-par-de-bes" >}})),
habría el conjunto de estados se separaría en dos uno asociado a {{< katex >}}
q_0 {{< /katex >}} y otro a {{< katex >}} q_1 {{< /katex >}}.

<center>
{{< figure src="../sigma.svg" title="Ilustración de partición de sigma para lenguaje número par de bes" >}}
</center>


