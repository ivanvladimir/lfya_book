---
weight: 21
title: "Revisando nuestro modelo"
description: "Revisamos nuestro modelo de caja negra junto con Σ*"
---


En el capítulo anterior presentamos nuestra abstracción de máquina
computacional como una caja negra que recibe un entrada compuesta de una cadena
y produce una salida que determina si para esa cadena se pudo hacer algo "útil" o
no. En el capítulo anterior también presentamos el concepto de cadena. No sólo
eso sino que se presentó el lenguaje {{< katex >}}\Sigma^* {{< /katex >}} que
contiene a todas las cadenas posibles compuestas por símbolos de un alfabeto {{< katex  
>}}\Sigma{{< /katex >}}.

¿Qué pasa si pasamos todo el conjunto {{< katex >}}\Sigma^*{{< /katex >}} a
una máquina computacional? La máquina, según como esté programada va a
determinar que unas cadenas de este conjunto serán útiles mientras que otras no.

<center>
{{< figure src="../cajanegra.svg" title="Abstración de máquina computacional cuando se le presenta todas las cadenas posibles de un alfabeto" >}}
</center>

El resultado de ese experimento es que efectivamente nuestra abstracción estaría
separando al lenguaje {{< katex >}}\Sigma^*{{< /katex >}} en dos lenguajes:

1. El lenguaje {{< katex >}}L{{< /katex >}} conformado por cadenas que fueron
   útiles para una caja específica.
1. El lenguaje {{< katex >}}\overline{L}{{< /katex >}} conformado por cadenas que 
   no fueron útiles para una caja específica.

Esta observación es muy importante porque permite apreciar que podemos establecer una relación entre el
conjunto de palabras que son útiles para una máquina específica y las piezas
internas de esta, por lo que serán estas piezas internas las que van a determinar si una cadena es
útil o no.

Si ponemos atención es fácil ver que {{< katex >}}\overline{L}{{< /katex >}} es
el complemento de {{< katex >}}L{{< /katex >}}. 

<center>
{{< figure src="../cajanegra2.svg" title="Abstración de máquina computacional separa en dos el lenguaje de todas las cadenas" >}}
</center>

























