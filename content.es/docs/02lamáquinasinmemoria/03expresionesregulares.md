---
weight: 23
title: "Expresiones regulares"
description: "Se define la notación: Expresiones Regulares"
---

Las expresiones regulares son una <mark>relajación</mark> en la notación de lenguajes
regulares. El objetivo es hacer más visible el patrón de concatenación, para
lograr esto se omiten las llaves de la notación de conjuntos y se cambia el
símbolo de la unión por un símbolo _+_.

De nuevo para la definición formal recurrimos a una definición recursiva.

### Expresiones regulares básicas

1. {{< katex >}}\emptyset{{< /katex >}} representa al lenguaje vacío
1. {{< katex >}}\varepsilon {{< /katex >}} representa al lenguaje de la cadena
   vacía.
1. {{< katex >}}a{{< /katex >}} donde {{<
   katex >}} a\in \Sigma {{< /katex >}} representa al
   lenguaje de un símbolo del alfabeto. 

Cabe recordar que estos casos por definición son lenguajes regulares, en su
notación como expresión regular

### Expresiones regulares para operaciones 

1. {{< katex >}}L_1 + L_2{{< /katex >}} representa la unión de dos lenguajes.
1. {{< katex >}} L_1 L_2{{< /katex >}} representa la concatenación.
1. {{< katex >}}L^*{{< /katex >}} representa la cerradura estrella de un
   lenguaje.
1. {{< katex >}}(L){{< /katex >}} representa la priorización por paréntesis de
   un lenguaje.

#### Ejemplo: número de _bes_ pares

Con esto en mente, la expresión regular para las cadenas que tienen número par
de _bes_ con {{< katex >}}\Sigma = \{a, b\} {{< /katex >}} queda como:

{{< katex display >}}(a^∗ba^∗ba^∗)^∗+a^* {{< /katex >}} 

Alternativamente

{{< katex display >}}a^∗(ba^∗ba^∗)^∗ {{< /katex >}} 

o

{{< katex display >}}(a^∗ba^∗b)^*a^∗ {{< /katex >}} 


#### Ejemplo: Penúltimo símbolo una _a_

La notación de expresiones regulares nos permite enfocarnos en <mark>el
patrón</mark> que define a las cadenas de nuestro lenguaje. El siguiente
ejemplo muestra como se puede construir una expresión regular para el lenguaje
de todas las cadenas cuyo último símbolo sea una _a_ con {{< katex >}}\Sigma = \{a, b\} {{< /katex >}} :

1. Cualquier cosa 🞄 dos últimas posiciones (para que exista penúltima posición)
2. Cualquier cosa 🞄 penúltima posición 🞄 última posición
3. Cualquier cosa 🞄 a 🞄 última posición
4. _a_ o _b_ tantas veces queramos 🞄 a 🞄 _a_ o _b_
5.  {{< katex >}}a+b  {{< /katex >}}tantas veces queramos 🞄 a 🞄 {{< katex >}}
    a+b {{< /katex >}}
6. {{< katex >}} (a+b)^*  {{< /katex >}}🞄 a 🞄 {{< katex >}} a+b {{< /katex >}}

Finalmente este patrón lo podemos representar con la ER:

{{< katex display >}} (a+b)^*a(a+b) {{< /katex >}} 

¿Cómo luciría la ER para todas las cadenas que comienzan con una _a_ con {{< katex >}}\Sigma = \{a, b\} {{< /katex >}}?

{{< details title="Respuesta" >}}

{{< katex display >}} a(a+b)^* {{< /katex >}} 

{{< /details >}}

