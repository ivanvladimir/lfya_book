---
weight: 26
title: "Formas de saber si un lenguaje es regular"
description: "Se enumeran las formas para saber si un lenguaje es regular"
---

Hasta este momento estás son las formas que hemos visto para saber que un
lenguaje es regular:

1. Es un _lenguaje regular básico_ o lo podemos _componer a través de las
   operaciones_ de unión, concatenación, cerradura estrella o priorización con
   paréntesis.

1. Diseñamos una _expresión regular_ que lo represente

1. Diseñamos un _Automata finito_ que lo acepte

Considerando estas opciones, cómo sabemos que el lenguaje compuesto por cadenas
que comienzan con _a_ con {{< katex >}}\Sigma = \{a,b\}{{< /katex >}} es
regular:

{{< details title="Respuesta" >}}

1. Surge de la secuencia de operaciones {{< katex >}}\{a\}{{< /katex >}} concatenada con {{< katex >}}\{a,b\}^*{{< /katex >}}
2. La representa la expresión regular:

{{< katex display >}}a(a+b)^*{{< /katex >}}

3. Lo acepta el autómata

<center>
{{< figure src="../a_loquesea.svg" title="Autómata finito para cadenas que comienzan con a" >}}
</center>

{{< /details >}}

