---
weight: 24
title: "Autómatas finitos"
description: "Se definen a los autómatas finitos"
---

En este momento vamos a hacer un cambio de paradigma, y vamos a presentar
nuestra primera máquina computacional que denominaremos <mark>Autómata Finito
(determinístico)</mark> (AF o AFD). El propósito de un autómata finito es _procesar_ una cadena
símbolo del alfabeto por símbolo del alfabeto hasta consumir toda la cadena;
dependiendo en qué estado se encuentra se determina si la cadena se acepta (es
decir, si fue útil). 

### Autómata finito (determinístico)

<div class="definition">
Un autómata finito es una tupla {{< katex >}}(Q,\Sigma,q_0,A,\delta){{< /katex >}} donde:


* {{< katex >}}Q{{< /katex >}} es un conjunto de estados finitos
* {{< katex >}}\Sigma{{< /katex >}} es un alfabeto
* {{< katex >}}q_0{{< /katex >}} es un estado que denominaremos inicial donde
    {{< katex >}}q_0 \in Q{{< /katex >}}
* {{< katex >}}A{{< /katex >}} es un conjunto de estados que denominaremos
    finales donde {{< katex >}}A \subset Q{{< /katex >}}
* {{< katex >}}\delta{{< /katex >}} es una función de transición que cumple con:
    {{< katex >}}\delta:Q \times \Sigma \rightarrow Q{{< /katex >}}
</div>

### Ejemplo: Número par de bes

En forma de tupla el autómata finito que acepta a cadenas con un número par de
_bes_ queda definido como:

{{< katex display >}}
\left(\{q_0,q_1\},\{a,b\},q_0,\{q_0\},\delta\right) 
{{< /katex >}}

Donde

{{< katex display >}}
\delta = \begin{cases} 
	(q_0,a) \rightarrow q_0  \\ 
	(q_0,b) \rightarrow q_1  \\ 
	(q_1,a) \rightarrow q_1  \\ 
	(q_1,b) \rightarrow q_0  \\ 
\end{cases}
{{< /katex >}}

De <mark>forma gráfica</mark> el autómata se representa de esta forma:

<center>
{{< figure src="../bes_pares.svg" title="Número par de bes" >}}
</center>

Los círculos y sus etiquetas representan a los estados, en este caso dos. Uno de
estos estados tiene la propiedad de ser inicial, marcado aquí con una flecha que
no proviene de ningún lado. Todos los estados potencialmente pueden tener la
propiedad de ser final, marcado aquí por un doble círculo. Los símbolos del
alfabeto lo usamos para marcar las transiciones (arcos). Las transiciones son la
definen la función de transición, se lee: "Si estoy en estado _q_ y llega un
símbolo _a_, transicionó de ese estado al que me indique la flecha".

#### Variantes

<center>
{{< figure src="../bes_pares1.svg" title="No número no par de bes (impar de bes)" >}}
</center>


{{< katex display >}}
\left(\{q_0,q_1\},\{a,b\},q_0,\{q_1\},\delta\right) 
{{< /katex >}}

Donde

{{< katex display >}}
\delta = \begin{cases} 
	(q_0,a) \rightarrow q_0  \\ 
	(q_0,b) \rightarrow q_1  \\ 
	(q_1,a) \rightarrow q_1  \\ 
	(q_1,b) \rightarrow q_0  \\ 
\end{cases}
{{< /katex >}}

¿Qué cadenas acepta este autómata finito?
{{< details title="Respuesta">}}

Acepta a las cadenas con número impar de cantidad de _bes_, es decir el
complemento de nuestro lenguaje _número par de bes_ 

{{< /details >}}


<center>
{{< figure src="../bes_pares2.svg" title="Número par de bes y no número par de bes, es decir todas las cadenas" >}}
</center>

{{< katex display >}}
\left(\{q_0,q_1\},\{a,b\},q_0,\{q_0,q_1\},\delta\right) 
{{< /katex >}}

Donde

{{< katex display >}}
\delta = \begin{cases} 
	(q_0,a) \rightarrow q_0  \\ 
	(q_0,b) \rightarrow q_1  \\ 
	(q_1,a) \rightarrow q_1  \\ 
	(q_1,b) \rightarrow q_0  \\ 
\end{cases}
{{< /katex >}}

¿Qué cadenas acepta este autómata finito?
{{< details title="Respuesta">}}

Acepta todas las posibles cadenas, porque no importa si tiene par o impar, de
todas formas la cadena resulta "útil".

{{< /details >}}


### Procesando cadenas con AF

Un problema con nuestra definición hasta este momento es que la función de
transición solo procesa un símbolo a la vez. Para que un AF pueda procesar una
cadena tenemos que extenderlo con la _función de transición extendida_ para la
que usaremos la notación:

{{< katex display >}}
\delta^*
{{< /katex >}}

Esta se define de forma recursiva de la siguiente forma:

{{< katex display >}}
\delta^*=\begin{cases} 
	\delta^*(q,\epsilon)=q & q \subseteq Q \\ 
	\delta^*(q,wa)=\delta(\delta^*(q,w),a) &q \subseteq Q, w \subseteq \Sigma^*, a \subseteq \Sigma  \\ 
	\end{cases}
{{< /katex >}}

Si el {{< katex >}}\delta^*(w) \in A{{< /katex >}} entonces _w_ es aceptada por el autómata.

### Ejemplo: AF par de _bes_ con la cadena _abbaa_

{{< katex display >}}
\begin{array}{rl}
\delta^*(q_0,abbaa) & = \delta(\delta^*(q_0,abba),a)\\
& = \delta(\delta(\delta^*(q_0,abb),a),a)\\
& = \delta(\delta(\delta(\delta^*(q_0,ab)b),a),a)\\
& = \delta(\delta(\delta(\delta(\delta^*(q_0,a),b),b),a),a)\\
& = \delta(\delta(\delta(\delta(\delta(\delta^*(q_0,\epsilon),a),b),b),a),a)\\
& = \delta(\delta(\delta(\delta(\delta(q_0,a),b),b),a),a)\\
& = \delta(\delta(\delta(\delta(q_0,b),b),a),a)\\
& = \delta(\delta(\delta(q_1,b),a),a)\\
& = \delta(\delta(q_0,a),a)\\
& = \delta(q_0,a)\\
& = q_0 \\
\end{array}
{{< /katex >}}


En el caso de la cadena _abbaa_ {{< katex >}}\delta^*(abbaa) = q_0{{< /katex >}} 
donde {{< katex >}}q_0 \in A{{< /katex >}} entonces la cadena es aceptada por el autómata.

#### De forma gráfica

<center>
{{< figure src="../bes_pares.gif" title="Ilustración de procesamiento de abbaa" >}}
</center>

## El lenguaje aceptado por un AF

Un autómata finito **A** acepta al lenguaje _L_ si:

1. Si {{< katex >}}w\in L{{< /katex >}} es aceptada por {{< katex >}}A{{< /katex >}}
2. Si {{< katex >}}w\not\in L{{< /katex >}} no es aceptada por {{< katex >}}A{{< /katex >}}

<center>
{{< figure src="../cajanegra3.svg" title="Lenguajes aceptados por un AF" >}}
</center>

Es decir:

<center>
{{< figure src="../cajanegra4.svg" title="Lenguajes aceptados por un AF" >}}
</center>

## Teorema de Kleen

{{% theorem %}}
Un lenguaje _L_ sobre el alfabeto _Σ_ **es regular si y sólo si existe** un
_Autómata Finito_ con un alfabeto _Σ_ que acepte _L_.
{{% /theorem %}}
