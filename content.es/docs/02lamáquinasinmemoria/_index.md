---
weight: 2
title: "La máquina sin memoria"
bookCollapseSection: true
---

{{< section >}}

## Enlaces para explorar

* <i>[Teoría de CONJUNTOS con CHOCOLATES](https://www.youtube.com/watch?v=HPlF_fmzN34)</i>. (2021, mar 2). [Video]</br>
* <i>[Cantor's Infinity Paradox | Set Theory](https://www.youtube.com/watch?v=X56zst79Xjg
)</i>. (2018, oct 26). [Video]</br>
* <i>[The Importance of Set Theory | Silvia Jonas](https://www.youtube.com/watch?v=9n3QsegLU3U)</i>. (2019, ago 8). [Video]</br>
* <i>[Infinity is bigger than you think - Numberphile](https://www.youtube.com/watch?v=elvOZm0d4H0
)</i>. (2012, jul 6). [Video]</br>
* <i>[Introduction to Languages, Strings, and Operations](https://www.youtube.com/watch?v=miOofcAiINM
)</i>. (2020, apr 22). [Video]</br>
* <i>[Concatenación](https://en.wikipedia.org/wiki/Concatenation
)</i>. (s/f). [Wikipedia]</br>
* <i>[Kleene star](https://www.planetmath.org/kleenestar
)</i>. (s/f).</br>
* <i>[Clausura de Kleene](https://es.wikipedia.org/wiki/Clausura_de_Kleene)</i>. (s/f). [Wikipedia]</br>
* <i>[The Regular Expression Edition
On code, early neural networks, and once discredited AI
pioneers](https://whyisthisinteresting.substack.com/p/the-regular-expression-edition)</i>. (2021, Jun 9). [Entrada de blog]</br>
* <i>[Expresión regular](https://es.wikipedia.org/wiki/Expresi%C3%B3n_regular)</i>. (s/f). [Wikipedia]</br>
* <i>[Autómata finito](https://es.wikipedia.org/wiki/Aut%C3%B3mata_finito)</i>. (s/f). [Wikipedia]</br>
* <i>[Regular Languages: Deterministic Finite Automaton (DFA)](https://www.youtube.com/watch?v=PK3wL7DXuuw)</i>. (2020, abr 22). [Video]</br>


## Lecturas recomendadas

* Kleene, S. C. (1951). Representation of Events in Nerve Nets and Finite
    Automata. RAND PROJECT AIR FORCE SANTA MONICA CA. **Capítulo 1**. 
    [[Liga](https://apps.dtic.mil/sti/citations/ADA596138)]
    [[PDF](https://apps.dtic.mil/sti/pdfs/ADA596138.pdf)]
* Hopcroft, J. E., Motwani, R., Ullman, J. D. Introducción a la teoría de
    autómatas, lenguajes y computación 2a. edición Madrid Pearson Education, 2002. **Sección 2.2, Capítulo 3**
    [[BiDi/UNAM](http://pbidi.unam.mx:8080/login?url=http://search.ebscohost.com/login.aspx?direct=true&db=cat02025a&AN=lib.MX001001976666&lang=es&site=eds-live)]
* Carrasco Jiménez, R. C.; Rubio, J. C.; Zubizarreta, M. L. F. Capítulo 1:
    Lenguajes y computadores. Teoría de Lenguajes, Gramáticas y Autómatas Para
    Informáticos, [s. l.], p. 11–22, 2000. **Capítulo 2, 3 y 4**
    [[BiDi/UNAM](http://pbidi.unam.mx:8080/login?url=https://search.ebscohost.com/login.aspx?direct=true&db=ufh&jid=87PC&lang=es&site=eds-live)]
    [[Liga](https://www.digitaliapublishing.com/a/812/teoria-de-lenguajes--gramaticas-y-automatas-para-informaticos)]
* Giró J, Vazquez J, Meloni B, Constable L. Lenguajes Formales y Teoría de
    Autómatas. Alfaomega. **Capítulo 2 y 3**
    [[BiDi/UNAM](http://pbidi.unam.mx:8080/login?url=http://search.ebscohost.com/login.aspx?direct=true&db=cat02025a&AN=lib.MX001001966916&lang=es&site=eds-live)]
    [[Liga](https://libroweb.alfaomega.com.mx/book/lenguajes_formales_teoria_automatas)]
* Hernández Rodríguez LA, Jaramillo Valbuena S, Cardona Torres SA. Practique
    La Teoría de Autómatas y Lenguajes Formales. Ediciones Elizcom; 2010.
    **Capítulo 3 y 4**
    [[BiDi/UNAM](http://pbidi.unam.mx:8080/login?url=http://search.ebscohost.com/login.aspx?direct=true&db=cat02025a&AN=lib.MX001001974225&lang=es&site=eds-live)]
    [[Liga](https://www.proquest.com/docview/2135020663/D0AE11F098D846F9PQ/1)]
* Cantú Treviño, T. G., & Mendoza García, M. G. (2015). Teoría de autómatas :
    un enfoque práctico (Primera edición). Pearson Educación de México.
    **Capítulo 2 y 3**
    [[BiDi/UNAM](http://pbidi.unam.mx:8080/login?url=https://search.ebscohost.com/login.aspx?direct=true&db=cat02025a&AN=lib.MX001001976665&lang=es&site=eds-live)]
* Holt, D. F., Rees, S., & Röver, C. E. (2017). Groups, languages and
    automata. Cambridge University Press. **Capítulo 2 y 3**
    [[BiDi/UNAM](http://pbidi.unam.mx:8080/login?url=https://search.ebscohost.com/login.aspx?direct=true&db=cat02025a&AN=lib.MX001001959658&lang=es&site=eds-live)]
    [[Liga](https://www.cambridge.org/core/books/groups-languages-and-automata/D31C153F123E5364D1690F697A0F16F2)]
* Brena, R. (2013). Autómatas y lenguajes. McGraw-Hill Interamericana.
    **Capítulo 2 y 3**
  [[BiDi/UNAM](http://pbidi.unam.mx:8080/login?url=https://search.ebscohost.com/login.aspx?direct=true&db=cat02025a&AN=lib.MX001001698825&lang=es&site=eds-live)]
* Crespi Reghizzi S, Breveglieri L, Morzenti A. Formal Languages and
    Compilation. Second edition. Springer; 2013. **Sección 2.3, 3.2. 3.3 y 3.4**
    [[BiDi/UNAM](http://pbidi.unam.mx:8080/login?url=http://search.ebscohost.com/login.aspx?direct=true&db=cat02025a&AN=lib.MX001001645742&lang=es&site=eds-live)]
    [[Liga](https://www.springer.com/gp/book/9783030048785)]


