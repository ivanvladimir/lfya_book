---
weight: 73
title: "Otras formas normales"
description: "Se presenta la Forma Normal de Greibach para GLC y la Forma Normal
de Kudura para GDC"
---

### FNG

Toda GLC se puede transformar en una gramática en la Forma Normal de Greibach que
requiere que las reglas tomen la siguiente forma:

{{< katex display >}} 
\begin{array}{ll}
A \rightarrow &  aA_1\ldots A_m \\ 
S \rightarrow &  \varepsilon \\ 
\end{array}
{{< /katex >}} 

### FNK

Toda GDC se puede transformar en una gramática en la Forma Normal de Kudura que
requiere que las reglas tomen la siguiente forma de gramáticas monotónicas:

{{< katex display >}} 
\begin{array}{ll}
AB \rightarrow &  CD \\ 
A \rightarrow &  BC \\ 
A \rightarrow &  B \\ 
A \rightarrow &  a \\ 
\end{array}
{{< /katex >}} 

