---
weight: 75
title: "Autómata de Doble Pila"
description: "Se presenta el concepto de Autómata de Doble Pila"
---


Una forma natural de aumentar el poder computacional de nuestra AP es aumentar
el número de pilas, en particular podemos agregar una más en el denominado
Autómata de Doble Pila


### ADP


<div class="definition">
Un <mark>Autómata de Doble Pila (ADP)</mark> es una tupla {{< katex
>}}(Q,\Sigma,\Gamma,q_0,Z_0,A, \delta ){{< /katex >}} donde:

* {{< katex >}}Q{{< /katex >}} es un conjunto de estados finitos
* {{< katex >}}\Sigma{{< /katex >}} es un alfabeto de símbolos terminales
* {{< katex >}}\Gamma{{< /katex >}} es un alfabeto de la pila
* {{< katex >}}q_0{{< /katex >}} es un estado que denominaremos inicial donde
    {{< katex >}}q_0 \in Q{{< /katex >}}
* {{< katex >}}Z_0{{< /katex >}} es un símbolo de la pila que denominaremos
    inicial de la pila donde
    {{< katex >}}Z_0 \in \Gamma{{< /katex >}}
* {{< katex >}}A{{< /katex >}} es un conjunto de estados que denominaremos
    finales donde {{< katex >}}A \subset Q{{< /katex >}}
* {{< katex >}}\delta{{< /katex >}} es una función de transición que cumple con:
    {{< katex display >}}\delta:Q \times (\Sigma\cup \{\varepsilon\}) \times \Gamma \times \Gamma \rightarrow Q \times \Gamma^* \times \Gamma^* {{< /katex >}}
    
</div>

#### Ejemplo de ADP

El siguiente autómata reconoce al lenguaje formado de cadenas {{< katex>}}a^nb^nc^n{{< /katex >}}
para cuando _n>0_:

<center>
{{< figure src="../anbncn.svg" title="ADP para cadenas aⁿbⁿcⁿ con n>0" >}}
</center>

Al igual que con los AF, al comenzar el análisis se comienza en el estado
inicial y de ahí se tienen que poner atención a cuatro aspectos, el estado en el
que se está, el símbolo en la cadena que está siendo analizado, el símbolo que
se encuentra hasta arriba de la primer pila y el símbolo que se encuentra hasta
arriba de la segunda pila. 

<center>
{{< figure src="../anbncn.gif" title="Análisis para la cadena aaabbbccc" >}}
</center>


