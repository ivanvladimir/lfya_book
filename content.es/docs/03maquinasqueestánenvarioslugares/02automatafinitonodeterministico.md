---
weight: 32
title: "Autómata Finito No Determinístico (AFND)"
description: "Se define el tipo de Autómata Finito No Determinístico"
---

Una limitante del [Autómata Finito]({{<ref "../02lamáquinasinmemoria/04autómatafinito/">}}) su diseño es complicado y algunas veces no
intuitivo. El diseñador de autómatas finitos se tiene concentrar en la esencia
de lo que representa cada estado de un autómata, y tiene que identificar
aquellos que permitan realizar el proceso de aceptación de una cadena de forma
correcta. 

Por otro lado como vimos algunas veces diseñar una especificación de un lenguaje
puede resultar muy directo. Surge la duda de por qué no tener un mecanismo más
cercano a las [Expresiones Regulares]({{< ref "../02lamáquinasinmemoria/03expresionesregulares/" >}}), para esto vamos a recurrir a aumentar la flexibilidad de los
autómatas finitos, el primer paso será permitir al autómata estar en más de un
estado, mientras que en el segundo caso permitiremos un caso especial en la
transición de un autómata.


### Definición

<div class="definition">
Un Autómata Finito No Determinístico es una tupla {{< katex >}}(Q,\Sigma,q_0,A,\delta){{< /katex >}} donde:


* {{< katex >}}Q{{< /katex >}} es un conjunto de estados finitos
* {{< katex >}}\Sigma{{< /katex >}} es un alfabeto
* {{< katex >}}q_0{{< /katex >}} es un estado que denominaremos inicial donde
    {{< katex >}}q_0 \in Q{{< /katex >}}
* {{< katex >}}A{{< /katex >}} es un conjunto de estados que denominaremos
    finales donde {{< katex >}}A \subset Q{{< /katex >}}
* {{< katex >}}\delta{{< /katex >}} es una función de transición que cumple con:
    <mark> {{< katex >}}\delta:Q \times \Sigma \rightarrow 2^Q{{< /katex >}}
    </mark>
</div>


#### El conjunto potencia

El conjunto {{< katex >}}2^Q{{< /katex >}} representa a todas los conjuntos
posibles que podamos hacer con los estados (i.e., [conjunto
potencia](https://es.wikipedia.org/wiki/Conjunto_potencia)). Es decir la función de transición ya
no regresa un sólo estado, sino que en la definición de un _AFND_ la función de
transición regresa un conjunto de estados, cualquier conjunto de estados que se
puedan formar con los estados.

#### Ejemplo: Máquina de chicles

Supongamos que queremos diseñar una máquina dispensadora de chicles, dónde estos
cuestan _5_ pesos. Supongamos que para esa máquina solo existen
monedas con valor de _1_, _2_ y _5_ pesos. Un _AFND_ que acepta todos los
posibles pagos que suman _5_ y que por lo tanto tendrían que hacer que la
máquina dispensadora otorgara un chicle sería: 

<center>
<a name="maquinas_independientes"></a>
{{< figure src="../chicles_independientes.svg" title="AFs para recibir los pagos de forma independiente" >}}
</center>

Sin embargo esta solución no resulta en una sola máquina, sino en __¡¡¡nueve
máquinas!!!__. Un "cambio" pequeño que permite cambiar esta situación sería proponer
la siguiente máquina:

{{< hint info >}}
**Nombres de estados**

Para cada una de estas máquinas el estado codifica la cantidad de monedas
que ha visto la máquina hasta cierto momento por ejemplo:  _q<sub>21</sub>_
señala que se han visto  _dos_ monedas de _1_ peso, _q<sub>11-12</sub>_ señala
que se han visto _una_ moneda de _1_ peso y _una_ moneda de _2_ pesos.

{{< /hint >}}



<center>
{{< figure src="../full.svg" title="AFND para máquina de pago de chicles" >}}
</center>

Como es evidente este AFND no puede confundirse con AF, porque del estado {{< katex >}}q_0{{< /katex >}} podemos llegar a varios estados, ya sea con una moneda de _un_ peso o de _dos_ pesos. 


{{< hint info >}}
**Nombres de estados**

En esta ocasión los nombres de los estados agregan información sobre qué
máquina independiente es la original. Por ejemplo el estado  _q<sub>21/4</sub>_
señala el estado donde ya han pagado con dos monedas de
un peso que originalmente pertenecía a la cuarta [máquina
independiente]({{< ref "#maquinas_independientes" >}}).

{{< /hint >}}

### Función de transición extendida para AFNDs

Al igual que hicimos para los Autómatas Finitos es necesario definir una función
de [transición extendida]({{< ref "../02lamáquinasinmemoria/04autómatafinito/#procesando-cadenas-con-af">}}) que tome como entrada una cadena y determine a que
estados finales se llegan. Para lograr esto volvemos a recurrir a una definición
recursiva de la siguiente forma:

{{< katex display >}}
\delta^*=\begin{cases} 
                \delta^*(q,\epsilon)=\{q\} & q \in Q \\ 
                                \delta^*(q,wa)= \bigcup\limits_{r \in
                                \delta^*(q,w)}\delta(r,a) &  q,r \subseteq Q, w
                                \subseteq \Sigma^*, a \subseteq \Sigma  \\ 
                                                \end{cases}
{{< /katex >}}

Otra vez tenemos <mark>un caso base</mark> (con la cadena vacía) y <mark>un caso recursivo</mark> (con una
cadena que podemos dividir en *prefijo* y *sufijo* de un sólo símbolo). El caso base simplemente nos
regresa como conjunto el estado inicial, mientras que el caso recursivo pospone
la evaluación del último símbolo con los estados posibles que se llegan a través
del sufijo.

#### Ejemplo función de transición extendida

¿Nuestro autómata de Máquina de chicles acepta el pago _1_, _2_ y _2_?

<br/>
{{< katex display>}}\begin{array}{rl}
\delta^*(q_0,122) & =\\
&=\bigcup\limits_{r_1\in\delta^*(q_0,12)}\delta(r_1,2)\\
&=\bigcup\limits_{r_1\in\bigcup\limits_{r_2\in\delta^*(q_0,1)}\delta(r_2,2)}\delta(r_1,2)\\
&=\bigcup\limits_{r_1\in\bigcup\limits_{r_2\in\bigcup\limits_{r_3\in\delta^*(q_0,\epsilon)}\delta(r_3,1)}\delta(r_2,2)}\delta(r_1,2)\\
&=\bigcup\limits_{r_1\in\bigcup\limits_{r_2\in\bigcup\limits_{r_3\in\{q_0\}}\delta(r_3,1)}\delta(r_2,2)}\delta(r_1,2)\\
&=\bigcup\limits_{r_1\in\bigcup\limits_{r_2\in\bigcup\limits_{r_3\in\{q_0\}}\delta(r_3,1)}\delta(r_2,2)}\delta(r_1,2)\\
&=\bigcup\limits_{r_1\in\bigcup\limits_{r_2\in\delta(q_0,1)}\delta(r_2,2)}\delta(r_1,2)\\
&=\bigcup\limits_{r_1\in\bigcup\limits_{r_2\in\{ q_{11/1},q_{11/2},q_{11/3},q_{11/4},q_{11/5}  \}}\delta(r_2,2)}\delta(r_1,2)\\
&=\bigcup\limits_{r_1\in\{ \delta(q_{11/1},2) \cup \delta(q_{11/2},2) \cup \delta(q_{11/3},2)  \cup \delta(q_{11/4},2)  \cup \delta(q_{11/5},2)  \}}\delta(r_1,2)\\
&=\bigcup\limits_{r_1\in\{ \emptyset \cup \{q_{11-12/2}\} \cup \{q_{11-12/3}\}  \cup \emptyset  \cup \emptyset  \}}\delta(r_1,2)\\
&=\bigcup\limits_{r_1\in\{ q_{11-12/2},q_{11-12/3}\} \}}\delta(r_1,2)\\
&=\delta(q_{11-12/2},2) \bigcup \delta(q_{11-12/3},2) \\
&=\{q_5 \} \bigcup \emptyset \\
&=\{q_5 \}\\
\end{array}
{{< /katex >}} 



De manera gráfica se ve así:
<center>
{{< figure src="../full.gif" title="AFND para máquina de pago de chicles aceptanto la cadena '122'" >}}
</center>

### Lenguaje aceptado por un AFND

Para el AFND {{< katex >}}M=(Q,\Sigma,q_0,A,\delta){{< /katex >}}

La cadena {{< katex >}}w\in \Sigma^*{{< /katex >}}se acepta si:

* {{< katex >}}\delta^*(q_0,w) \bigcap A \not = \emptyset{{< /katex >}}

_L(M)_ es el lenguaje conformado por cadenas aceptadas por _M_
