---
weight: 33
title: "AFND → AF"
description: "Se explica el proceso para transformar todo AFND a AF"
---

Un aspecto sorprendente de los AFND es que son equivalentes a los AF; esto quiere
decir que <mark>todo AFND podrá ser reducido a un AF</mark>.

### Reducción

La reducción recae en el hecho que todo AFND tiene un número finito de estados,
y podemos imaginarnos que una combinación de estados de un AFND sera equivalente
a un estado de un AF. De esta forma cuando en un AFND estamos en una combinación de
estados y llega un símbolo, y este nos lleva a otra combinación, en realidad nos
estamos moviendo de un estado a otro en el AF a través del mismo símbolo.

Por supuesto, existe un problema exponencial ya que los posibles estados en AF
dado un AFND son todas las posibles combinaciones de los estados de este. Tratar
de hacer el análisis de dicho número de combinaciones no es práctico. Sin
embargo, existe un proceso más directo que no requiere explorar todo este
espacio y es a través de codificar los estados del AF con un código posicional
de los estados del AFND. Un _1_ en el código del AF representa que
el estado en el AFND está activo, si hay más unos en el código todos estos
estados se encuentran activos, mientas que un _0_ representa que el estado
correspondiente en el AFND no está activo. 

#### El procedimiento

Suponiendo un AFND {{< katex >}}(Q,\Sigma,q_0,A,\delta){{< /katex >}} el
siguiente procedimiento genera la tabla de transición de un AF {{< katex>}}(Q',\Sigma,q'_0,A',\delta'){{< /katex >}} equivalente:
 
1. Definir el código para los estados nuestro AF, cada estado {{< katex >}}q\in Q{{< /katex >}}
   define una posición de esa codificación.
2. Agregamos en nuestra tabla una fila para donde estado origen es {{< katex >}}q_0{{< /katex >}}
   cuyo código es la posición del estado inicial activada
   con un _1_ mientras que el restos de las posiciones son cero.
3. Por cada símbolo {{< katex >}}a \in \Sigma {{< /katex >}} construir la
   codificación en la columna correspondiente al símbolo _a_ dónde se activan
   los estados a los que se llega con dicho símbolo partiendo del estado origen
   correspondiente a la fila.
4. Por cada codificación nueva que aparezca en las columnas agregar una fila.
5. Si existe un código que no haya sido analizada regresar a 3.

Al final podemos renombrar las codificaciones de los estados con nombres más
compactos. 

Finalmente, _Q_ será conformado por todos los estados identificados, {{< katex >}}q'_0{{< /katex >}}
será el estado correspondiente a la codificación creada en el paso 2; _A_ estará
conformada por todos los estados cuya codificación tiene activa una posición
correspondiente a un estado final del original AFND; y finalmente la tabla
de transición representa {{< katex >}}q'_0{{< /katex >}}.

#### Ejemplo 

Supongamos el siguiente orden para nuestros estados del AFND:

{{< katex display >}}
 q_{0}, q_{11/1}, q_{11/5}, q_{11/3}, q_{11/4}, q_{11/2}, q_{12/7}, q_{12/8}, q_{12/6}, q_{5}, q_{21/1}, q_{31/1}, \\
 q_{41/1}, q_{11_12/2}, q_{11_12/3}, q_{21_12/3}, q_{21/4}, q_{21_12/4}, q_{21/5}, q_{31/5}, q_{22/6}, q_{11_12/7},\\
 q_{11_12/8}, q_{21_12/8}
{{< /katex >}}

Con este orden podemos codificar el estado inicial como:

{{< katex display >}}
100000000000000000000000 
{{< /katex >}}

Con este estado inicial podemos averiguar a dónde llegamos si recibimos cada uno
de los símbolos del alfabeto:

<div class="font-tiny">

| Estado | 1 | 2 | 5 |
|:------:|:-:|:-:|:-:|
| 100000000000000000000000 | 011111000000000000000000 | 000000111000000000000000 | 000000000100000000000000 |

</div>

En este caso las tres codificaciones resultantes son nuevas, por lo que hay que
agregarlas y calcular a dónde podemos llegar si llega uno de los símbolos del
alfabeto:

<div class="font-tiny">

| Estado | 1 | 2 | 5 |
|:------:|:-:|:-:|:-:|
| 100000000000000000000000 | 011111000000000000000000 | 000000111000000000000000 | 000000000100000000000000 |
| 011111000000000000000000 | | | |
| 000000111000000000000000 | | | |
| 000000000100000000000000 | | | |

</div>

Si hacemos el calculo para estas nuevas filas obtenemos:

<div class="font-tiny">

| estado | 1 | 2 | 5 |
|:------:|:-:|:-:|:-:|
| 100000000000000000000000 | 011111000000000000000000 | 000000111000000000000000 | 000000000100000000000000 |
| 011111000000000000000000 | 000000000010000010100000 | 000000000000011000000000 | 000000000000000000000000 |
| 000000111000000000000000 | 000000000000000000000110 | 000000000000000000001000 | 000000000000000000000000 |
| 000000000100000000000000 | 000000000000000000000000 | 000000000000000000000000 | 000000000000000000000000 |

</div>

Es importante notar que la codificación con sólo ceros, representa que con ese símbolos y el estado en el que se encuentra no se puede llegar ningún estado, es un estado de error.

La tabla final resultante es:

<div class="font-tiny">

| estado | 1 | 2 | 5 |
|:------:|:-:|:-:|:-:|
| 100000000000000000000000 | 011111000000000000000000 | 000000111000000000000000 | 000000000100000000000000 |
| 011111000000000000000000 | 000000000010000010100000 | 000000000000011000000000 | 000000000000000000000000 |
| 000000111000000000000000 | 000000000000000000000110 | 000000000000000000001000 | 000000000000000000000000 |
| 000000000100000000000000 | 000000000000000000000000 | 000000000000000000000000 | 000000000000000000000000 |
| 000000000010000010100000 | 000000000001000000010000 | 000000000000000001000000 | 000000000000000000000000 |
| 000000000001000000010000 | 000000000000100000000000 | 000000000100000000000000 | 000000000000000000000000 |
| 000000000000100000000000 | 000000000100000000000000 | 000000000000000000000000 | 000000000000000000000000 |
| 000000000000011000000000 | 000000000000000100000000 | 000000000100000000000000 | 000000000000000000000000 |
| 000000000000000100000000 | 000000000100000000000000 | 000000000000000000000000 | 000000000000000000000000 |
| 000000000000000001000000 | 000000000100000000000000 | 000000000000000000000000 | 000000000000000000000000 |
| 000000000000000000001000 | 000000000100000000000000 | 000000000000000000000000 | 000000000000000000000000 |
| 000000000000000000000110 | 000000000000000000000001 | 000000000100000000000000 | 000000000000000000000000 |
| 000000000000000000000001 | 000000000100000000000000 | 000000000000000000000000 | 000000000000000000000000 |

</div>


<center>
{{< figure src="../dfa_full.svg" title="El DFA resultante de la codificación" >}}
</center>


Por supuesto la tabla es incomprensible, pero podemos renombrar los estados de
la siguiente forma:

| estado | nuevo nombre | 
|:------:|:-:|
| 100000000000000000000000 | q_0 |
| 011111000000000000000000 | q_1 |
| 000000111000000000000000 | q_2 |
| 000000000100000000000000 | q_3 |
| 000000000010000010100000 | q_4 |
| 000000000001000000010000 | q_5 |
| 000000000000100000000000 | q_6 |
| 000000000000011000000000 | q_7 |
| 000000000000000100000000 | q_8 |
| 000000000000000001000000 | q_9 |
| 000000000000000000001000 | q_10 |
| 000000000000000000000110 | q_11 |
| 000000000000000000000001 | q_12 |


Resultando:

| estado | 1 | 2 | 5 |
|:------:|:-:|:-:|:-:|
| ⟶ q_0 | q_1 | q_2 | q_3 |
|q_1 | q_4 | q_5 | |
|q_2 |q_6 | q_7 | | 
|q_3 |  |  |  |    
|q_4 |q_8 | q_9 | | 
|q_5 |q_10 |   q_3 | |
|q_6 |q_11 |   q_3 | |
|q_7 |q_3  |   | |
|q_8 |q_12 |   q_3 | | 
|q_9 |q_3  | | |
|q_10|    q_3   | | |  
|q_11|    q_3   | | |
|q_12|    q_3   | | |

Finalmente, si lo graficamos queda de la siguiente forma:

<center>
<a name="AF_unchicle"></a>
{{< figure src="../dfa_chicles.svg" title="AF producto de la reducción de un AFND" >}}
</center>

<center>
{{< figure src="../dfa.gif" title="AF procesando la cadena 122" >}}
</center>
