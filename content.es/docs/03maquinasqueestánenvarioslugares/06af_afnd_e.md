---
weight: 36
title: "AF → AFND-ε"
description: "Se explica el proceso para transformar todo AF a un AFND-ε"
---

Un aspecto interesante de los AF es que son equivalentes a los AFND-ε; esto quiere
decir que <mark>todo AF podrá ser reducido a un AFND-ε</mark>, y por la
equivalencia de AFND con AF y AFND-ε con AFND, los tres tipos de máquinas
finitas **AFND-ε**, **AFNS** y **AF** <mark>son equivalentes</mark>

### Reducción

Esta reducción es trivial, sólo hay que agregar una columna a nuestra tabla de
transición para la transición ε donde toda la columna va al conjunto vacía.

| estado | 1 | 2 | 5 | ε | 
|:------:|:-:|:-:|:-:|:-:|
| q | {_}  | {_} | {_} | ∅ |


#### Ejemplo

Supongamos el siguiente autómata finito,

<center>
{{< figure src="../unos_cero.svg" title="AF par 0*1" >}}
</center>

Con la siguiente tabla de transición

| estado | 0 | 1 | 
|:------:|:-:|:-:|
| q_0 | q_0  | q_1 | 
| q_1 |  |  |

Su reducción como AFND-ε es:

| estado | 0 | 1 | ε | 
|:------:|:-:|:-:|:-:|
| q_0 | {q_0}  | {q_1} | ∅ |
| q_1 | ∅ | ∅ | ∅ |

