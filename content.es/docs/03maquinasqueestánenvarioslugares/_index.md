---
weight: 3
title: "Las máquinas que están en varios lugares"
bookCollapseSection: true
---

{{< section >}}

## Enlaces para explorar


* <i>[Rabin on writing “Finite Automata and their Decision Problems” with Dana Scott](https://www.youtube.com/watch?v=mso0tqgHupA)</i>. (2020, oct 20) [Video]
* <i>[The Regular Expression Edition/Why is this
    interesting?](https://whyisthisinteresting.substack.com/p/the-regular-expression-edition?utm_source=pocket_mylist)</i>.
    (2021, 9 jun)
* <i>[determinism](http://foldoc.org/deterministic)</i>. (1995, sep 22)
* <i>[nondeterminism](http://foldoc.org/nondeterminism)</i>. (1995, apr 13)
* <i>[Regular Languages: Nondeterministic Finite Automaton (NFA)](https://www.youtube.com/watch?v=W8Uu0inPmU8)</i>. (2020, abr 22). [Video]</br>
* <i>[Hopcroft on Formal Languages and Their Relationship to Automata](https://www.youtube.com/watch?v=7y9NvR2v6XA)</i>. (2020, jun 22). [Video]</br>
* <i>[Autómata finito no determinista](https://es.wikipedia.org/wiki/Aut%C3%B3mata_finito_no_determinista)</i>. (s/f). [Wikipedia]</br>
* <i>[Nondeterministic finite automaton](https://en.wikipedia.org/wiki/Nondeterministic_finite_automaton)</i>. (s/f). [Wikipedia]</br>
* <i>[Algoritmo de Thompson](https://es.wikipedia.org/wiki/Algoritmo_de_Thompson)</i>. (s/f) [Wikipedia]</br>
* <i>[Thompson's construction](https://en.wikipedia.org/wiki/Thompson%27s_construction)</i>. (s/f) [Wikipedia]</br>
* <i>[Comparison of regular-expression engines](https://en.wikipedia.org/wiki/Comparison_of_regular-expression_engines)</i>. (s/f) [Wikipedia]</br>
* <i>[re/python](https://docs.python.org/3/library/re.html)</i>. (s/f)
* <i>[Regular Expression HOWTO/python](https://docs.python.org/3/howto/regex.html)</i>. (s/f)
* <i>[The Perfect URL Regular Expression](http://urlregex.com/)</i>. (s/f)
* <i>[In search of the perfect URL validation regex](https://mathiasbynens.be/demo/url-regex)</i>. (s/f)
* <i>[ dperini/regex-weburl.js ](https://gist.github.com/dperini/729294)</i>. (2018, sep 12)

## Lecturas recomendadas

* M. O. Rabin and D. Scott, "Finite Automata and Their Decision Problems," in
    _IBM Journal of Research and Development_, vol. 3, no. 2, pp. 114-125, April
    1959, doi: 10.1147/rd.32.0114.
    [[Liga]](https://doi.org/10.1147/rd.32.0114)
    [[PDF]](https://www.researchgate.net/profile/Dana-Scott-3/publication/230876408_Finite_Automata_and_Their_Decision_Problems/links/582783f808ae950ace6cd752/Finite-Automata-and-Their-Decision-Problems.pdf)
* Thompson, K. (1968). Programming techniques: Regular expression search
    algorithm. Communications of the ACM, 11(6), 419-422.
    [[Liga]](https://doi.org/10.1145/363347.363387)
    [[PDF]](https://dl.acm.org/doi/pdf/10.1145/363347.363387)
* Hopcroft, J. E., Motwani, R., Ullman, J. D. Introducción a la teoría de
    autómatas, lenguajes y computación 2a. edición Madrid Pearson Education, 2002. **Sección 2.2, Capítulo 3**
    [[BiDi/UNAM](http://pbidi.unam.mx:8080/login?url=http://search.ebscohost.com/login.aspx?direct=true&db=cat02025a&AN=lib.MX001001976666&lang=es&site=eds-live)]
* Carrasco Jiménez, R. C.; Rubio, J. C.; Zubizarreta, M. L. F. Capítulo 1:
    Lenguajes y computadores. Teoría de Lenguajes, Gramáticas y Autómatas Para
    Informáticos, [s. l.], p. 11–22, 2000. **Capítulo 2, 3 y 4**
    [[BiDi/UNAM](http://pbidi.unam.mx:8080/login?url=https://search.ebscohost.com/login.aspx?direct=true&db=ufh&jid=87PC&lang=es&site=eds-live)]
    [[Liga](https://www.digitaliapublishing.com/a/812/teoria-de-lenguajes--gramaticas-y-automatas-para-informaticos)]
* Giró J, Vazquez J, Meloni B, Constable L. Lenguajes Formales y Teoría de
    Autómatas. Alfaomega. **Capítulo 4**
    [[BiDi/UNAM](http://pbidi.unam.mx:8080/login?url=http://search.ebscohost.com/login.aspx?direct=true&db=cat02025a&AN=lib.MX001001966916&lang=es&site=eds-live)]
    [[Liga](https://libroweb.alfaomega.com.mx/book/lenguajes_formales_teoria_automatas)]
* Hernández Rodríguez LA, Jaramillo Valbuena S, Cardona Torres SA. Practique
    La Teoría de Autómatas y Lenguajes Formales. Ediciones Elizcom; 2010.
    **Capítulo 4**
    [[BiDi/UNAM](http://pbidi.unam.mx:8080/login?url=http://search.ebscohost.com/login.aspx?direct=true&db=cat02025a&AN=lib.MX001001974225&lang=es&site=eds-live)]
    [[Liga](https://www.proquest.com/docview/2135020663/D0AE11F098D846F9PQ/1)]
* Cantú Treviño, T. G., & Mendoza García, M. G. (2015). Teoría de autómatas :
    un enfoque práctico (Primera edición). Pearson Educación de México.
    **Capítulo 3**
    [[BiDi/UNAM](http://pbidi.unam.mx:8080/login?url=https://search.ebscohost.com/login.aspx?direct=true&db=cat02025a&AN=lib.MX001001976665&lang=es&site=eds-live)]
* Brena, R. (2013). Autómatas y lenguajes. McGraw-Hill Interamericana.
    **Capítulo 2 y 3**
  [[BiDi/UNAM](http://pbidi.unam.mx:8080/login?url=https://search.ebscohost.com/login.aspx?direct=true&db=cat02025a&AN=lib.MX001001698825&lang=es&site=eds-live)]
* Crespi Reghizzi S, Breveglieri L, Morzenti A. Formal Languages and
    Compilation. Second edition. Springer; 2013. **Sección 3.5 a 3.8**
    [[BiDi/UNAM](http://pbidi.unam.mx:8080/login?url=http://search.ebscohost.com/login.aspx?direct=true&db=cat02025a&AN=lib.MX001001645742&lang=es&site=eds-live)]
    [[Liga](https://www.springer.com/gp/book/9783030048785)]


