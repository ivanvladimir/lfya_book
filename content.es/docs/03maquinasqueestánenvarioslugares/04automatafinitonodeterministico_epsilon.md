---
weight: 34
title: "Autómata Finito No Determinístico con transición épsilon (AFND-ε)"
description: "Se definie el tipo de Autómata Finito No Determinístico"
---

La flexibilidad lograda por el AFND fue muy buena, sin embargo al diseñar un
AFND tenemos que seguir pensando el procesamiento de la cadenas. Sería deseable
todavía mayor flexibilidad, esto lo lograremos al permitir una transición a
través de la cadena vacía, es decir podremos transiciones entre estados aún
cuando no hayamos visto un símbolo en la cadena.


### Definición

<div class="definition">
Un Autómata Finito No Determinístico con transición épsilon es una tupla {{< katex >}}(Q,\Sigma,q_0,A,\delta){{< /katex >}} donde:

* {{< katex >}}Q{{< /katex >}} es un conjunto de estados finitos
* {{< katex >}}\Sigma{{< /katex >}} es un alfabeto
* {{< katex >}}q_0{{< /katex >}} es un estado que denominaremos inicial donde
    {{< katex >}}q_0 \in Q{{< /katex >}}
* {{< katex >}}A{{< /katex >}} es un conjunto de estados que denominaremos
    finales donde {{< katex >}}A \subset Q{{< /katex >}}
* {{< katex >}}\delta{{< /katex >}} es una función de transición que cumple con:
    <mark>{{< katex >}}\delta:Q \times (\Sigma \cup \{\epsilon \})  \rightarrow 2^Q{{< /katex >}}</mark>
</div>


#### Ejemplo: Máquina de chicles con múltiples pagos

Qué pasa ahora si deseamos que nuestra máquina de chicles ya no sólo acepte un
sólo pago de un chicle sino de múltiples pagos ¿Tendríamos que rediseñar nuestra
máquina? Una solución es utilizar la extensión de la cadena vacía como
transición, como se ilustra a continuación:

<center>
{{< figure src="../ndfa_e_multiple.svg" title="AFND-ε para múltiples pagos de chicles" >}}
</center>

¿Cuál es la diferencia con el <a href='{{< ref "03afnd_af#af_unchicle">}}'>AF de la sección anterior?</a>
{{< details title="Respuesta" open=false >}}
Existe una nueva transición de _q_3_ a _q_0_ a través de la cadena vacía, _ε_.
Esto permite que una vez realizado un pago por un chicle se pueda proceder a
otro.
{{< /details >}}


### Función de transición extendida para AFND-ε

Al igual que hicimos para los Autómatas Finitos y los Autómatas Finitios No Determinísticos es necesario definir una función
de transición extendida que tome como entrada una cadena y determine a que
estados finales se llegan. Para lograr esto volvemos a recurrir a una definición
recursiva de la siguiente forma:

{{< katex display >}}
\delta^*=\begin{cases} 
                \delta^*(q,\epsilon)= exp_{\epsilon}(\{q\}) & q \in Q \\ 
                                \delta^*(q,wa)= exp_{\epsilon}
                                (\bigcup\limits_{r \in
                                \delta^*(q,w)}\delta(r,a)) &  q,r \subseteq Q, w
                                \subseteq \Sigma^*, a \subseteq \Sigma  \\ 
                                                \end{cases}
{{< /katex >}}

Otra vez tenemos un caso base (con la cadena vacía) y un caso recursivo (con una
cadena que podemos dividir en prefijo y sufijo de un sólo símbolo).  sin embargo
ahora tenemos que definir una nueva función auxiliar _exp_  (expansión por épsilon)

### Calculo de expansión por épsilon

El objetivo de la función de expansión es recolectar los estados extras a los que se pueda llegar a través de la cadena vacía (ε).

1. Para cada {{< katex >}}q \in S{{< /katex >}} incluir in {{< katex >}}S{{< /katex >}} todo {{< katex >}}\delta(q,\epsilon){{< /katex >}}
2. Repetir hasta que {{< katex >}}S{{< /katex >}} no cambie


#### Ejemplo función de transición extendida

¿Nuestro autómata de Máquina de chicles acepta el pago _5_, _2_, _1_ y _2_?

<br/>
{{< katex display >}}
\begin{array}{rl}
\delta^*(q_0,5212)
&=exp_{\epsilon}(\bigcup\limits_{r_1\in\delta^*(q_0,521)}\delta(r_1,2))\\
&=exp_{\epsilon}(\bigcup\limits_{
							r_1\in exp_{\epsilon}(\bigcup\limits_{r_2\in\delta^*(q_0,52))}
							\delta(r_2,1))}
			\delta(r_1,2))\\
&=\exp_{\epsilon}(\bigcup\limits_{
							r_1\in exp_{\epsilon}(\bigcup\limits_{
									r_2\in	exp_{\epsilon}(\bigcup\limits_{r_3\in\delta^*(q_0,5)}\delta(r_3,2))}
							\delta(r_2,1))}
			\delta(r_1,2))\\
&=\exp_{\epsilon}(\bigcup\limits_{
							r_1\in\exp_{\epsilon}(\bigcup\limits_{
									r_2\in	exp_{\epsilon}(\bigcup\limits_{
											r_3\in exp_{\epsilon}(\bigcup\limits_{r_4\in\delta^*(q_0,\epsilon)}\delta(r_4,5))}
									\delta(r_3,2))}
							\delta(r_2,1))}
			\delta(r_1,2))\\
&=\exp_{\epsilon}(\bigcup\limits_{
							r_1\in\exp_{\epsilon}(\bigcup\limits_{
									r_2\in	exp_{\epsilon}(\bigcup\limits_{
											r_3\in exp_{\epsilon}(\bigcup\limits_{
												r_4\in exp_{\epsilon}(\{q_0\}) }\delta(r_4,5))}
									\delta(r_3,2))}
							\delta(r_2,1))}
			\delta(r_1,2))\\
&=\exp_{\epsilon}(\bigcup\limits_{
							r_1\in\exp_{\epsilon}(\bigcup\limits_{
									r_2\in	exp_{\epsilon}(\bigcup\limits_{
											r_3\in exp_{\epsilon}(\bigcup\limits_{
												r_4\in \{q_0\} }\delta(r_4,5))}
									\delta(r_3,2))}
							\delta(r_2,1))}
			\delta(r_1,2))\\
&=\exp_{\epsilon}(\bigcup\limits_{
							r_1\in\exp_{\epsilon}(\bigcup\limits_{
									r_2\in	exp_{\epsilon}(\bigcup\limits_{
											r_3\in exp_{\epsilon}(\delta(q_0,5))}
									\delta(r_3,2))}
							\delta(r_2,1))}
			\delta(r_1,2))\\
&=\exp_{\epsilon}(\bigcup\limits_{
							r_1\in\exp_{\epsilon}(\bigcup\limits_{
									r_2\in	exp_{\epsilon}(\bigcup\limits_{
											r_3\in exp_{\epsilon}( \{q_3\} )}
									\delta(r_3,2))}
							\delta(r_2,1))}
			\delta(r_1,2))\\
&=\exp_{\epsilon}(\bigcup\limits_{
							r_1\in\exp_{\epsilon}(\bigcup\limits_{
									r_2\in	exp_{\epsilon}(\bigcup\limits_{
											r_3\in \{q_0,q_3\}}
									\delta(r_3,2))}
							\delta(r_2,1))}
			\delta(r_1,2))\\
&=\exp_{\epsilon}(\bigcup\limits_{
							r_1\in\exp_{\epsilon}(\bigcup\limits_{
									r_2\in	exp_{\epsilon}(
									\delta(q_0,2) \cup \delta(q_3,2))}
							\delta(r_2,1))}
			\delta(r_1,2))\\
&=\exp_{\epsilon}(\bigcup\limits_{
							r_1\in\exp_{\epsilon}(\bigcup\limits_{
									r_2\in	exp_{\epsilon}(
									\{q_2\} ))}
							\delta(r_2,1))}
			\delta(r_1,2))\\
&=\exp_{\epsilon}(\bigcup\limits_{
							r_1\in\exp_{\epsilon}(\bigcup\limits_{
									r_2\in \{q_2\} }
							\delta(r_2,1))}
			\delta(r_1,2))\\
&=\exp_{\epsilon}(\bigcup\limits_{
							r_1\in\exp_{\epsilon}(
							\delta(q_2,1))}
			\delta(r_1,2))\\
&=\exp_{\epsilon}(\bigcup\limits_{
							r_1\in \exp_{\epsilon}(
							\{q_6\} )}
			\delta(r_1,2))\\
&=\exp_{\epsilon}(\bigcup\limits_{
							r_1\in \{q_6\}}
			\delta(r_1,2))\\
&=\exp_{\epsilon}(\{q_3 \})\\
&=\{q_0,q_3\}
\end{array}
{{< /katex >}}

De forma gráfica luce de la siguiente forma:

<center>
{{< figure src="../ndfa_e_multiple.gif" title="AFND para máquina de pago de chicles aceptanto la cadena '5212'" >}}
</center>

### Lenguaje aceptado por un AFND-ε

Para el AFND-ε {{< katex >}}M=(Q,\Sigma,q_0,A,\delta){{< /katex >}}

La cadena {{< katex >}}w\in \Sigma^*{{< /katex >}}se acepta si:

* {{< katex >}}\delta^*(q_0,w) \bigcap A \not = \emptyset{{< /katex >}}

_L(M)_ es el lenguaje conformado por cadenas aceptadas por _M_
