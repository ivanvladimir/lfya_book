---
weight: 31
title: "Especificación contra proceso"
description: "Se explora la diferencia entre expresiones regulares y autómatas"
---

Por el Teorema de Kleene sabemos que tanto Expresiones Regulares (una relajación
en la notación de los Lenguajes Regulares) son equivalentes a los Autómatas
Finitos (no determinístico). El que sean equivalentes quiere decir que
representan exactamente al mismo tipo de lenguajes, los 
[Lenguajes Regulares]({{< ref "../02lamáquinasinmemoria/02leguajesregulares/">}}). Este es una característica muy
importante de nuestro modelo computacional. 

Esta correspondencia tiene varias implicaciones:

* Si sabemos de un Lenguaje Regular _L_ está garantizado que debe existir
    un autómata finito.
* Si sabemos sobre un Autómata Finito _A_ está garantizado que debe existir un
    Lenguaje Regular y por lo tanto una Expresión Regular que los represente.

### ¿Cúal es la diferencia en _ER_ y Autómata?

¿Si ambos ER y Autómatas representan el mismo tipo de lenguajes porque
necesitamos a ambos, porque no quedarnos con una de la opciones? La respuesta
tiene que ver con que [_ER_]({{< ref "../02lamáquinasinmemoria/03expresionesregulares/">}}) 
y [_AF_]({{< ref "../02lamáquinasinmemoria/04autómatafinito/">}}) son dos aspectos diferentes de los
Lenguajes Regulares. Las Expresiones Regulares especifican el patrón que debe
seguir las cadenas que pertenecen al lenguaje, mientras que el Autómata Finito define
el proceso que se tiene que seguir para aceptar a una cadena del lenguaje. 

#### Ejemplo de especificación vs proceso

Supongamos el lenguaje de las cadenas conformadas por _aes_ y _bes_ en donde
todas las cadenas contiene la subcadena _abb_. 

{{< katex display >}}L=\{abb,aabba,babba,babbb,\ldots\}{{< /katex >}}

La _ER_ que representa a este lenguaje es {{< katex >}}(a+b)^*abb(a+b)*{{< /katex >}}.
Si nos detenemos a analizar esta _ER_ podemos ver que nos está especificando la
forma que tienen que ser las cadenas, nos dice que puede venir cualquier cosa {{< katex >}}(a+b)^*{{< /katex >}},
es decir cualquier cadena compuesta por repeticiones de _aes_ o _bes_. Sin
embargo, en algún momento debe aparecer la cadena _abb_ para después ser seguida
por cualquier cosa. El infijo de _abb_ garantiza que nuestra cadena tenga esa
subcadena, no nos dice nada de cómo será procesada, por ejemplo con la cadena
_abbabb_ no queda claro cual de las dos apariciones de _abb_ se alinea con el
infijo.

<center>
{{< figure src="../er_alineación.png" title="Alineación de la ER con una misma cadena" >}}
</center>

Por otro lado un autómata para este lenguaje es:

<center>
{{< figure src="../contiene_abb.svg" title="AF para lenguaje de cadenas que contiene a la subcadena 'abb'" >}}
</center>


Se puede apreciar que el autómata sigue una estrategia diferente, describe una
secuencia de decisiones para determinar si en la cadena ya ocurrió la subcadena
_abb_; al comenzar si viene una _be_ no hay chances de que ocurra la cadena
_abb_, pero si viene una _a_ potencialmente hemos encontrado la _a_ de la
subcadena, sin embargo si llega otra _a_ quiere decir que la _a_ pasada no
pertenece a 
nuestra subcadena ya que el patrón _aa_ no es el que buscamos confirmar; sin
embargo la nueva _a_ podría ser el inicio de nuestro patrón _abb_; si llega una
_b_ se confirmaría que llevamos dos símbolos de nuestro patrón, sólo faltará una
siguiente _b_ y podríamos estar seguros que ocurrió el patrón que buscábamos y
después de eso ya no importa lo que llegue, siempre estaremos en un estado
final. Sin embargo, si llegaba una _a_ se rompe el patrón y tenemos que regresar
a considerar que el patrón buscado apenas está comenzado con esa nueva _a_, por
eso regresamos al estado __q_1__ en lugar que el inicial.

