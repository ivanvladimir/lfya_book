---
weight: 35
title: "AFND-ε → AFND"
description: "Se explica el proceso para transformar todo AFND-ε a AFND"
---

Un aspecto interesante de los AFND-ε es que son equivalentes a los AFND; esto quiere
decir que <mark>todo AFND-ε podrá ser reducido a un AFND</mark>, y por la
equivalencia de AFND entre AF, <mark>todo AFND-ε podrá ser reducido a un AF</mark>

### Reducción

En esta reducción, lo único que tenemos que hacer es quitar la transición
épsilon. En este momento nuestra función de transición luce así:

| estado | 1 | 2 | 5 | ε |
|:------:|:-:|:-:|:-:|:-:|
| ⟶q_0	| {q_1} | {q_2} | {q_3} | ∅ |
| q_1	| {q_4} | {q_5}	| ∅ | ∅ |
| q_2	| {q_6} | {q_7} | ∅ | ∅ |		
| q_3	| ∅ | ∅ | ∅ | {q_0} |		
| q_4	| {q_8} | {q_9} | ∅ | ∅ |	
| q_5	| {q_10} | {q_3} | ∅ | ∅ |	
| q_6	| {q_11} | {q_3} | ∅ | ∅ |	
| q_7	| {q_3} |  ∅ | ∅ | ∅ |	
| q_8	| {q_12} | {q_3} |	∅ | ∅ |	
| q_9	| {q_3} |  ∅ | ∅ |	∅ |	
| q_10	| {q_3} | ∅ | ∅ | ∅ |		
| q_11	| {q_3} |	∅ | ∅ | ∅ |	
| q_12	| {q_3} | ∅ | ∅ | ∅ |			


Para lograr esto solo tenemos que evaluar la función de transición extendida por
cada uno de los símbolos y por cada uno de los estados en nuestro AFND-ε, de la
siguiente forma:

| estado | δ*(q,"1") | δ*(q,"2")  | δ*(q,"5")  | 
|:------:|:-:|:-:|:-:|
| ⟶q_0	|  δ*(q_0,"1") |  δ*(q_0,"2") | δ*(q_0,"3") | 

Es decir suponemos estar en el estado de la fila, y tratamos al símbolo como si
fuera una cadena y registramos a dónde podemos llegar:

| estado | δ*(q,"1") | δ*(q,"2")  | δ*(q,"5")  | 
|:------:|:-:|:-:|:-:|
| ⟶q_0	|  {q_1} |  {q_2} | {q_3,q_0} | 

Esto se hace para todas las filas resultando en el AFND

| estado | 1 | 2 | 5 | 
|:------:|:-:|:-:|:-:|
| ⟶q_0 | {q_1} | {q_2} | {q_0,q_3}
| q_1	| {q_4} | {q_5}	| ∅ | ∅ |
| q_2	| {q_6} | {q_7} | ∅ | ∅ |		
| q_3	| ∅ | ∅ | ∅ | {q_0,q_3} |		
| q_4	| {q_8} | {q_9} | ∅ | ∅ |	
| q_5	| {q_10} | {q_3,q_0} | ∅ | ∅ |	
| q_6	| {q_11} | {q_3,q_0} | ∅ | ∅ |	
| q_7	| {q_3} |  ∅ | ∅ | ∅ |	
| q_8	| {q_12} | {q_3,q_0} |	∅ | ∅ |	
| q_9	| {q_3,q_0} |  ∅ | ∅ |	∅ |	
| q_10	| {q_3,q_0} | ∅ | ∅ | ∅ |		
| q_11	| {q_3,q_0} |	∅ | ∅ | ∅ |	
| q_12	| {q_3,q_0} | ∅ | ∅ | ∅ |			


Que de forma gráfica se puede visualizar así:

<center>
{{< figure src="../ndfa_multiple.svg" title="NDFA producto de la reducción de AFND-ε " >}}
</center>

## Extra

Por supuesto si tenemos un AFND, podemos reducirlo a un AF siguiendo el procedimiento
en [AFND → AF]({{< ref "docs/03maquinasqueestánenvarioslugares/03afnd_af#reducción" >}}). El resultado es:

<center>
{{< figure src="../dfa_multiple.svg" title="DFA producto de la reducción de AFND " >}}
</center>

<a id="pago_22222"></a>
Existe un error con nuestra máquina, no se puede pagar con 5 monedas de dos
pesos ¿Cómo se podría arreglar?
{{< details title="Respuesta" open=false >}}

Agregando una transición de _q_7_, donde se han pagado con dos monedas de _2_
pesos, a _q_1_ a través de la tercera moneda de _2_ pesos, de esta forma en la
segunda ronda lleva un equivalente a _6_ pesos. La siguiente animación muestra
el pago con cinco monedas de _2_ pesos.

<center>
{{< figure src="../dfa_multiple_22222.gif" title="AFND para máquina de pago de chicles aceptanto la cadena '5212'" >}}
</center>

{{< /details >}}
