---
weight: 62
title: "Lenguajes de palíndromos"
description: "Se presenta los diferentes lenguajes de palíndromos"
---

Un palíndromo es un texto que se lee igual de izquierda a derecha que de derecha
a izquierda. Los palíndromos han sido explorado desde la literatura, un
excelente expositor de los palíndromos en español es el escrito argentino [Juan
Filloy](https://es.wikipedia.org/wiki/Juan_Filloy), algunos ejemplos son:

* _Acaso hubo búhos acá_
* _Adán y raza, azar y nada_
* _Allí va Ramón y no maravilla_
* _Al reparto, otra perla_
* _Amad a la dama_
* _Amada dama_

Para más ejemplos hechos por Juan Filloy visitar [Los palíndromos de Juan
Filloy](https://eljineteinsomne2.blogspot.com/2007/09/los-palindromos-de-juan-filloy.html).
Para ver más ejemplos de palíndromos buscar con el
[hashtag](https://twitter.com/search?q=%23palindromo&src=typed_query&f=live) en la plataforma social
Twitter.

### Gramáticas para lenguajes de palíndromos

Desde el punto de vista de lenguajes formales, se pueden definir los siguientes
lenguajes de palíndromos y sus gramáticas dado
un alfabeto {{< katex >}} \Sigma = \{a,b\}{{< /katex >}}, donde {{< katex >}} w^r
{{</katex >}} es un cadena escrita en reversa:

* Palíndromos pares
 {{< katex display >}} ww^r {{< /katex >}}
 {{< katex display >}}
 \begin{array}{ll} 
 S \rightarrow & aSa | bSb | \varepsilon  
 \end{array}
 {{< /katex >}}


* Palíndromos impares
 {{< katex display >}} wcw^r, c \in \Sigma{{< /katex >}}

 {{< katex display >}}
 \begin{array}{ll} 
 S \rightarrow & aSa | bSb | a | b  
 \end{array}
 {{< /katex >}}

* Palíndromos pares e impares
 {{< katex display >}} w=w^r {{< /katex >}}
 {{< katex display >}}
 \begin{array}{ll} 
 S \rightarrow & aSa | bSb | a | b | \epsilon  
 \end{array}
 {{< /katex >}}


* Palíndromos con marca en medio
 {{< katex display >}} wmw^r {{< /katex >}}
 {{< katex display >}}
 \begin{array}{ll} 
 S \rightarrow & aSa | bSb | m  
 \end{array}
 {{< /katex >}}


Como podemos ver todos las gramáticas son no ambiguas, ya que generan siempre un
árbol de derivación para todos sus derivaciones, lo que muestra que estos
lenguajes son no ambiguos.


#### AP para _wwʳ_

El autómata para este lenguaje luce de la siguiente forma, palíndromos pares

<center>
{{< figure src="../wwr.svg" title="AP para wwʳ " >}}
</center>

{{< details "AP procesando la cadena 'abbbba'" >}} 

Al procesar la cadena se puede observar que el AP es no determinístico
<center>
{{< figure src="../wwr.gif" title="AP para wwʳ procesando abbbba" >}}
</center>
{{< /details >}}


#### AP para _wawʳ+wbwʳ_

El autómata para este lenguaje luce de la siguiente forma, palíndromos impares

<center>
{{< figure src="../wawr.svg" title="AP para wawʳ " >}}
</center>

{{< details "AP procesando la cadena 'abbabba'" >}} 

Al procesar la cadena se puede observar que el AP es no determinístico
<center>
{{< figure src="../wawr.gif" title="AP para wawʳ procesando abbabba" >}}
</center>
{{< /details >}}

#### AP para _w=wʳ_

El autómata para este lenguaje luce de la siguiente forma, palíndromos

<center>
{{< figure src="../w_wr.svg" title="AP para w=wʳ " >}}
</center>

{{< details "AP procesando la cadena 'abbabba'" >}} 

Al procesar la cadena se puede observar que el AP es no determinístico
<center>
{{< figure src="../w_wr.gif" title="AP para w=wʳ procesando abbabba" >}}
</center>
{{< /details >}}

#### AP para _wmwʳ_

El autómata para este lenguaje luce de la siguiente forma, palíndromos

<center>
{{< figure src="../wmwr.svg" title="AP para wmwʳ" >}}
</center>

{{< details "AP procesando la cadena 'abbmbba'" >}} 

Al procesar la cadena se puede observar que el AP es **determinístico**
<center>
{{< figure src="../wmwr.gif" title="AP para wmwʳ procesando abbmbba" >}}
</center>
{{< /details >}}


