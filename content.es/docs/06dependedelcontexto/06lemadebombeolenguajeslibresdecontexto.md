---
weight: 66
title: "Lema de bombeo para lenguajes libres de contexto"
description: "Se presenta el lema de bombeo para los lenguajes libres de contexto"
---

Sea _L_ un Lenguaje Libre de Contexto para todas la cadenas _w_  se
podrán descomponer en cinco partes _uvwxy_ tales que:

1. {{< katex >}} |vwx| \leq n {{< /katex >}}
2. {{< katex >}} vx \ne \varepsilon {{< /katex >}}, puesto de otra forma {{< katex >}} y > 1 {{< /katex >}}
3. Para todo {{< katex >}} k \geq 0 {{< /katex >}}, las cadenas con la forma {{< katex >}} uv^kwx^ky {{< /katex >}} también pertenecen a _L_


### Aplicación del lema de bombeo

El lema de bombeo se puede usar para demostrar que un lenguaje no es libre de contexto

1. Identificar el lenguaje libre de contexto _L_
2. Escoger una longitud _n_ para _xy_
3. Proponer una forma de cadena que dependa de _n_ pero que sea mayor que _n_
4. Particionar la cadena _w_ en _uvwxy_
5. Verificar que la partición cumpla con las restricciones:
   1. {{< katex >}} uvwxy = w {{< /katex >}}
   1. {{< katex >}} |vwx| \leq n {{< /katex >}}
   1. {{< katex >}} |vx| > 0 {{< /katex >}}
6. Generar cadenas bombeadas {{< katex >}} xy^kz \in L{{< /katex >}} 

Si no cumple, se demuestra que el lenguaje no es libre de contexto.

#### Ejemplo de uso del lema de bombeo

Dado {{< katex >}} \Sigma = \{a,b\} {{< /katex >}} demostrar que el lenguaje
formado por cadenas con la forma {{< katex >}} a^nb^nc^n {{< /katex >}} no es
libre de contexto. Para esto, vamos a seguir la aplicación del lemma de bombeo:

1. Identificar un lenguaje regular
   {{< katex >}} L =\{ w | w=a^nb^nc^n\} {{< /katex >}}

2. Escoger una longitud _n_
   Qué tal si en lugar de escoger un valor específico, escogemos cualquier valor, es
   decir _n_

3. Proponer una forma de cadena que dependa de _n_ pero que sea mayor que _n_
   Propongo: {{< katex >}} a^nb^nc^n {{< /katex >}}
    
4. Particionar la cadena _w_ en _uvwxy_, varios casos:
   * _vwx_ son aes {{< katex >}}u=\varepsilon,v=a^{\frac{n}{2}},w=\varepsilon,x=a^{\frac{n}{2}},y=b^nc^n{{< /katex >}}, no cumple porque al bombear de desbalancean las aes
   {{< katex display >}}
   (a^{\frac{n}{2}})^k(a^{\frac{n}{2}})^kb^nc^n
   {{< /katex >}}
   * _vwx_ son aes y bes {{< katex >}}u=a^{\frac{n}{2}},v=a^{\frac{n}{2}},w=\varepsilon,x=b^{\frac{n}{2}},y=b^{\frac{n}{2}}c^n{{< /katex >}}, no cumple porque al bombear de desbalancean las aes y bes
   {{< katex display >}}
   a^{\frac{n}{2}}(a^{\frac{n}{2}})^k(b^{\frac{n}{2}})^kb^{\frac{n}{2}}c^n
   {{< /katex >}}
   * _vwx_ son bes {{< katex >}}u=a^n,v=b^{\frac{n}{2}},w=\varepsilon,x=b^{\frac{n}{2}},y=c^n{{< /katex >}}, no cumple porque al bombear de desbalancean las bes
   {{< katex display >}}
   a^n(b^{\frac{n}{2}})^k(b^{\frac{n}{2}})^kc^n
   {{< /katex >}}
   * _vwx_ son bes y ces {{< katex >}}u=a^nb^{\frac{n}{2}},v=b^{\frac{n}{2}},w=\varepsilon,x=c^{\frac{n}{2}},y=c^{\frac{n}{2}}{{< /katex >}}, no cumple porque al bombear de desbalancean las bes y ces
   {{< katex display >}}
   a^nb^{\frac{n}{2}}(b^{\frac{n}{2}})^k(c^{\frac{n}{2}})^kc^{\frac{n}{2}}
   {{< /katex >}}
   * _vwx_ son bes {{< katex >}}u=a^nb^n,v=c^{\frac{n}{2}},w=\varepsilon,x=c^{\frac{n}{2}},y=\varepsilon{{< /katex >}}, no cumple porque al bombear de desbalancean las ces
   {{< katex display >}}
   (a^nb^n(c^{\frac{n}{2}})^k(c^{\frac{n}{2}})^k
   {{< /katex >}}

Por lo tanto el lenguaje formado de cadenas con la forma {{< katex >}} a^nb^nc^n {{< /katex >}} es no regular. 


### El lenguaje de las cadenas _aⁿbⁿcⁿ_

A diferencia de los lenguajes que hasta este momento habíamos visto, este
lenguaje pide algo que no habíamos visto antes. Dos partes del lenguaje dependen
del otra parte. La parte de _bes_ y _ces_ dependen de la parte de _aes_ ya que el número de
_aes_ define cuantas _bes_  y _ces_ habrá, y aunque tenemos un mecanismo para
pasar información, la pila, una vez que consumimos la información contenida en
ella, la perdemos para la siguiente parte, haciendo imposible que este
lenguaje sea posible de aceptar por un AP. 
