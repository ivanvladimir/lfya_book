---
weight: 64
title: "Simulación de G como AP"
description: "Se explica que es posible simular una G como un AP"
---

Dada una gramática {{< katex >}}G=(V,\Sigma,P,S){{< /katex >}} se puede crear una AP que lo simule  
{{< katex >}}(Q,\Sigma,\Gamma,q_0,Z_0,A,\delta){{< /katex >}} donde:

{{< katex display >}}
\begin{array}{ll}
			Q= & \{q_0,q_1,q_2\} \\
			\Gamma= & V\cup\Sigma\cup\{Z_0\}, Z_0\not\in (V\cup\Sigma) \\
			A= & \{q_2\}
\end{array}
{{< /katex >}}

Donde las transiciones se definen de la sigueinte forma:

1. {{< katex >}}\delta(q_0,\epsilon,Z_0)=\{(q_1,SZ_0\})\}{{< /katex >}}
1. {{< katex >}}\delta(q_1,\epsilon,Z_0)=\{(q_2,Z_0\})\}{{< /katex >}}
1. Para todo {{< katex >}}A\in V, \delta(q_1,\epsilon,A)=\{(q_1,\alpha\}) | A\rightarrow \alpha\}{{< /katex >}}
1. Para todo{{< katex >}}a\in\Sigma, \delta(q_1,a,a)=\{(q_1,\epsilon)\}{{< /katex >}}

De forma gráfica, el AP luce de la siguiente forma:

<center>
{{< figure src="../ap_glc_td.svg" title="Ilustración de cómo luce un AP que simula una G" >}}
</center>

### Ejemplo

Para la gramática de palíndromos {{< katex display >}} w=w^r {{< /katex >}}
 {{< katex display >}}
 \begin{array}{ll} 
 P \rightarrow & aPa | bPb | a | b | \epsilon  
 \end{array}
 {{< /katex >}}

Este es su AP que lo simula:

<center>
{{< figure src="../ap_glc_tdxr.svg" title="AP que simula a lenguaje de palíndromos" >}}
</center>


