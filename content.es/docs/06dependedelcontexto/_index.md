---
weight: 6
title: "Depende del contexto"
bookCollapseSection: true
---

{{< section >}}

## Enlaces para explorar

* <i>[Lema del bombeo para lenguajes libres del contexto](https://es.wikipedia.org/wiki/Lema_del_bombeo_para_lenguajes_libres_del_contexto)</i>. (s/f) [Wikipedia]
* <i>[Gramáticas sensibles al contexto](https://es.wikipedia.org/wiki/Gram%C3%A1ticas_sensibles_al_contexto)</i>. (s/f) [Wikipedia]
* <i>[Deterministic context-free language](https://en.wikipedia.org/wiki/Deterministic_context-free_language)</i>. (s/f) [Wikipedia]
* <i>[Deterministic pushdown automaton](https://en.wikipedia.org/wiki/Deterministic_pushdown_automaton)</i>. (s/f) [Wikipedia]
* <i>[The Optical Illusion
    Edition](https://whyisthisinteresting.substack.com/p/the-optical-illusion-edition)</i>. (2022, Ago 16) [Entrada de lista de correo]</br>
 

## Lecturas recomendadas

* Chomsky, N. (1963). "Formal properties of grammar". In Luce, R. D.; Bush, R.
    R.; Galanter, E. (eds.). Handbook of Mathematical Psychology. New York:
    Wiley. pp. 360–363.
    [[Liga]](https://archive.org/details/handbookofmathem017893mbp)
* Bar-Hillel, Y., Perles, M., & Shamir, E. (1961). On formal properties of
    simple phrase structure grammars. _Sprachtypologie und
    Universalienforschung_, 14, 143-172.
    [[Liga]](https://www.proquest.com/openview/fb41296047fb7453dcb1de182b4aa0b6/1?pq-origsite=gscholar&cbl=1817266)

* Bar-Hillel, Y., Perles, M., & Shamir, E. (1961). On formal properties of
    simple phrase structure grammars. _Sprachtypologie und
    Universalienforschung_, 14, 143-172.
    [[Liga]](https://www.proquest.com/openview/fb41296047fb7453dcb1de182b4aa0b6/1?pq-origsite=gscholar&cbl=1817266)
* Hopcroft, J. E., Motwani, R., Ullman, J. D. Introducción a la teoría de
    autómatas, lenguajes y computación 2a. edición Madrid Pearson Education,
    2002. **Capítulo 6 y 7**
    [[BiDi/UNAM](http://pbidi.unam.mx:8080/login?url=http://search.ebscohost.com/login.aspx?direct=true&db=cat02025a&AN=lib.MX001001976666&lang=es&site=eds-live)]
* Carrasco Jiménez, R. C.; Rubio, J. C.; Zubizarreta, M. L. F. Capítulo 1:
    Lenguajes y computadores. Teoría de Lenguajes, Gramáticas y Autómatas Para
    Informáticos, [s. l.], p. 11–22, 2000. **Capítulo 6 y 7**
    [[BiDi/UNAM](http://pbidi.unam.mx:8080/login?url=https://search.ebscohost.com/login.aspx?direct=true&db=ufh&jid=87PC&lang=es&site=eds-live)]
    [[Liga](https://www.digitaliapublishing.com/a/812/teoria-de-lenguajes--gramaticas-y-automatas-para-informaticos)]


