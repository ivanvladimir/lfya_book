---
weight: 67
title: "Gramática Dependiente del Contexto"
description: "Se presenta el concepto de las Gramáticas Dependientes del Contexto"
---

En la [sección anterior]({{< ref "06lemadebombeolenguajeslibresdecontexto#Lema-de-bombeo-para-lenguajes-libres-decontexto" >}}) identificamos al lenguaje formado por cadenas del tipo
{{< katex >}} a^nb^nc^n {{< /katex >}} que no es libre de contexto _¿Entonces qué es?_ Antes
de poder explicar más sobre este lenguaje vamos a presentar el concepto de
_gramáticas dependientes de contexto_, que nos ayudarán a
generar cadenas de este lenguaje.

### GLC

<div class="definition">
Una <mark>Gramática Dependiente de Contexto (GLDC)</mark> es una tupla {{< katex >}}(V,\Sigma,P,S){{< /katex >}} donde:


* {{< katex >}}V{{< /katex >}} es un alfabeto de <mark>símbolos no
    terminales</mark> o símbolos auxiliares
* {{< katex >}}\Sigma{{< /katex >}} es un alfabeto de <mark>símbolos
    terminales</mark>, que conforman nuestras cadenas
* {{< katex >}}P{{< /katex >}} es un <mark>conjunto de reglas</mark> que con la forma
    {{< katex >}}\gamma A \beta \rightarrow  \gamma \alpha \beta{{< /katex >}} donde 
    {{< katex >}}\alpha \in \left(\Sigma \cup V \right)^*, \gamma \in \left(\Sigma \cup V \right)^*, \beta \in \left(\Sigma \cup V \right)^*,{{< /katex >}}  
* {{< katex >}}S \in V{{< /katex >}} lo denominamos <mark>símbolo inicial</mark>
</div>


#### Ejemplo de GDC

Un ejemplo de Gramática Dependiente del Contexto es:

 {{< katex >}}\left(\{S\},\{a,b\},P,S \right){{< /katex >}} donde _P_:
 {{< katex display >}} 
 \begin{array}{lclclcl}
 & S & & \rightarrow & & abC &\\ 
 & S & & \rightarrow & & aSBC &\\ 
 & C & B & \rightarrow & & W & B\\ 
 W & B &  & \rightarrow & W & X & \\ 
 & W & X & \rightarrow & & B & X\\ 
 B & X &  & \rightarrow & B & C & \\ 
 b & B &  & \rightarrow & b & b & \\ 
 b & C &  & \rightarrow & b & c & \\ 
 c & C &  & \rightarrow & c & c & \\ 
 \end{array}
 {{< /katex >}} 

### Derivación

De forma similar que las GLC las GDC siguen un proceso de re-escritura, a un
camino de escritura de forma similar se le denomina derivación. A continuación
un ejemplo de derivación para la cadena {{< katex >}}a^nb^nc^n{{< /katex >}}.

 {{< katex display >}} 
 \begin{array}{rl}
\underline{S} \Rightarrow &  a\underline{S}BC\\
\Rightarrow & aa\underline{S}BCBC\\
\Rightarrow & aaab\underline{CB}CBC\\
\Rightarrow & aaab\underline{WB}CBC\\
\Rightarrow & aaab\underline{WX}CBC\\
\Rightarrow & aaab\underline{BX}CBC\\
\Rightarrow & aaabBC\underline{CB}C\\
\Rightarrow & aaabBC\underline{WB}C\\
\Rightarrow & aaabBC\underline{WX}C\\
\Rightarrow & aaabBC\underline{BX}C\\
\Rightarrow & aaabB\underline{CB}CC\\
\Rightarrow & aaabB\underline{WB}CC\\
\Rightarrow & aaabB\underline{WX}CC\\
\Rightarrow & aaab\underline{BB}CCC\\
\Rightarrow & aaabb\underline{BC}CC\\
\Rightarrow & aaabb\underline{bC}CC\\
\Rightarrow & aaabbb\underline{cC}C\\
\Rightarrow & aaabbbc\underline{cC}\\
\Rightarrow & aaabbbccc\\
\end{array}
{{< /katex >}}

### Lenguajes Dependientes del Contexto

Formalmente un lenguje _L_ generado por una
gramática dependiente del contexto _G_  se define como {{< katex >}} L=\{ w | S \Rightarrow^* w \} {{< /katex >}}
donde _S_ es el símbolo inicial de la gramática {{< katex >}}G=\left(V,\Sigma,P,S \right){{< /katex >}}. Es decir el lenguaje
de asociado a una GDC es el conjunto de todas las cadenas que tienen una
derivación partiendo del símbolo inicial de la gramática y a través de aplicar
una secuencia finita de re-escrituras siguiendo las reglas de producción.

## Algunas observaciones

Al final de esta sección podemos concentrar lo que sabemos en la siguiente tabla:

| Lenguaje | Gramática | Máquina | Ejemplo  |
|:--------:|:---------:|:-------:|:--------:|
| Lenguaje Dependientes del Contexto | {{< katex >}} \gamma A \beta \rightarrow \gamma \alpha \beta {{< /katex >}} | ?? | {{< katex >}} a^nb^nc^n {{< /katex >}} |
| Lenguaje Independiente del Contexto | {{< katex >}} A \rightarrow \alpha {{< /katex >}} | Autómata de Pila | {{< katex >}} a^nb^n {{< /katex >}} |
| Lenguaje Regular | {{< katex >}} A \rightarrow bC | d {{< /katex >}} | AF, AFND y AFND-ε  | {{< katex >}} a^n {{< /katex >}} |


Ahora podemos ver porque se denomina a los Lenguajes Independientes del
Contexto, como independientes, ya que a diferencia de los dependientes el
contexto no está definido _(γ y ꞵ)_, o mejor dicho es contexto muy particular donde _gamma_ es
_epsilon_ y _beta_ también. 

También es importante notar, que hasta este momento si nos fijamos en la tabla
los niveles de esta están definidos por la forma de las reglas. La forma de las
reglas se vuelven más específicas para los niveles más abajo. Dependientes del
contexto define cualquier contexto, independientes del contexto define un sólo
contexto, y regulares requiere una forma peculiar de _alfa_.


