---
weight: 61
title: "Resumen hasta ahora"
description: "Se reflexiona sobre lo que sabemos hasta ahora"
---

Es un buen momento para hacer un recuento de los conceptos que hemos visto hasta
el momento. 

* __Lenguajes__  Este es el concepto fundamental presentado en este curso, es un
    conjunto de cadenas.
* __Máquinas__  Son mecanismos computacionales que permiten procesar una cadena
    y determinar si pertenece o no al lenguaje asociado a dicha máquina.
* __Gramática__ Es un proceso de re-escritura basado en remplazo de reglas que
    permite generar cadenas de un lenguaje determinado.

Con esto en mente este es el mapa conceptual que sabemos en este momento:

| Lenguaje | Gramática | Máquina | Ejemplo  |
|:--------:|:---------:|:-------:|:--------:|
| Lenguaje Independiente del Contexto | {{< katex >}} A \rightarrow \alpha {{< /katex >}} | Autómata de Pila | {{< katex >}} a^nb^n {{< /katex >}} |
| Lenguaje Regular | {{< katex >}} A \rightarrow bC | d {{< /katex >}} | AF, AFND y AFND-ε  | {{< katex >}} a^n {{< /katex >}} |

Cabe recordar que los Lenguajes Independientes del Contexto contienen a los
Lenguajes Regulares; una prueba de esto es en la definición de la forma de las
reglas de producción de la Gramática Regular, estas cumplen también con la forma
para reglas de producción de Gramáticas Independientes del Contexto, solo pasa
que son más específicas.


