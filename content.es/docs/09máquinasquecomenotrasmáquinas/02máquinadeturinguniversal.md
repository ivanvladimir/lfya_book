---
weight: 92
title: "Máquina de Turing Universal"
description: "Se presenta a la MTU"
---

Una MT _M_ puede codificarse en cadena, y nada evita que esa cadena sea
analizada por otra MT _M'_, por ejemplo que determine cuantos estados hay la
máquina _M_. 

### Máquina de Turing Universal

Un caso interesante de MT que consume una cadena descripción estándar de una MT
y simula su ejecución. En 1937, Alan Turing en su
[artículo](https://londmathsoc.onlinelibrary.wiley.com/doi/abs/10.1112/plms/s2-42.1.230) "On Computable Numbers, with an
Application to the Entscheidungsproblem" escribió:

> It is possible to invent a single machine which can be used to compute any
computable sequence. If this machine _U_ is supplied with a tape on the beginning
of which is written the S.D ["standard description" of an action table] of some
computing machine _M_, then _U_ will compute the same sequence as _M_.

Que se puede traducir a:

> Es posible inventar una máquina que pueda ser usada para computar cualquier
secuencia computable. Si esta máquina _U_ se le provee con una cinta en la que al
principio se le escribe la descripción estándar de una tabla de acción de alguna
máquina _M_, entonces _U_ computará la misma secuencia que _M_.

A esta máquina _U_ se le denomina Máquina de Turing Universal ({{< katex >}}M_U{{<
/katex >}}).


### El lenguaje aceptado por MTU

La {{< katex >}}M_U{{< /katex >}} recibe como entrada una cadena _<m>_ que
representa a un MT _M_ seguida de una cadena _w_. El lenguaje aceptado por la
{{< katex >}}M_U{{< /katex >}} se define de la siguiente forma:

{{< katex display >}}
A_{MT}=\{ \langle \text{m} \rangle w | w \in L(M)\}
{{< /katex >}}

Es decir el {{< katex >}}A_{MT}{{< /katex >}} es el lenguaje conformado por
cadenas con dos partes, la primera define la función de transición de una MT y
la segunda es una cadena aceptada por esa MT.

<center>
{{< figure src="../mtu.svg" title="Máquina de Turing Universal" >}}
</center>


