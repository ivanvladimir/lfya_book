---
weight: 96
title: "El complemento de los no decidibles"
description: "Se ahonda en el concepto de complemento de no decidibles"
---

[Anteriormente]({{<ref
"../08lamáquinaconcinta/06reflexionsobrecomplementos/">}})
habíamos establecido que si un lenguaje era decidible entonces su complemento
también lo era. Sin embargo, con el descubrimiento le lenguajes y máquinas no
decidibles, surge la duda de cómo lucen sus complementos.


### Fuera de lo computable

El complemento de un lenguaje no decidible debe tener una naturaleza diferente
hasta lo que hemos visto ahora. Un lenguaje no decidible tendrá una MT que lo
acepte sin embargo no podemos garantizar que la máquina termine. 

Como
establecimos [antes]({{< ref
"05consecuenciadesernodecidible/#dentro-de-malas-noticias-un-poco-de-buenas-noticias">}}).
Una MT eventualmente va a poder parar si la cadena que analiza pertenece al
lenguaje, quiere decir para todas las cadenas del lenguaje las MT eventualmente
pararán aceptando a las cadenas. Para las cadenas no aceptadas, algunas veces
terminará o algunas veces se ciclará. En el complemento del lenguaje ocurrirá lo
contrario, para todas las cadenas que no pertenecen al lenguaje la MT parará (ya
que son las cadenas que en el complemento la MT aceptó y paró) y para las
cadenas que debe aceptar se quedará ciclada o determinará aceptar. 

Con esto en mente se determina que el complemento de los lenguajes RE
conforman a los lenguajes <mark>co-RE</mark>, es decir aquellos
lenguajes para los cuales no podemos determinar para una cadena su pertenencia al lenguaje, pero si su no pertenencia. 

### Visualizando 

La relación entre estos tipos de lenguajes se puede apreciar en la siguiente
figura.

<center>
{{< figure src="../viztlang.svg" title="Relación entre RE, co-RE y Rec" >}}
</center>

Vemos que en el caso de los lenguajes Recursivos (R)/decidibles se trata de la
intersección entre los lenguajes RE y co-RE. 

### ¿Qué hay afuera de RE y co-RE?

Resulta que hay problemas que no se pueden verificar la pertenencia de las
cadenas ya sea aceptadas o rechazadas. Por ejemplo el problema:

{{< katex display>}}
REGULAR = \{ \text{<m>} | L(M) \text{ es un lenguaje regular}\}
{{< /katex >}}

Es decir {{< katex >}}REGULAR{{< /katex >}} contiene todas las descripciones de máquinas de Turing que
representan a un lenguaje regular. Resulta que este lenguaje no es ni RE ni
co-RE cae fuera de lo computable. Y por lo tanto el complemento también 
{{< katex >}}\overline{REGULAR}{{< /katex >}} 

### Recursivo pero no Depediente del Contexto

Hasta ahora los ejemplos de MT o han sido Dependientes del Contexto, es decir se
podrían implementar con un ALF o son no decidibles (es decir RE pero no
Recursivos). Un ejemplo trivial de una MT que es decidible es una MT universal
M<sub>Ut</sub> que decide si otra MT acabará antes de cierto tiempo o número de pasos. Como
vemos esta MT debe ser en su naturaleza una MT porque tiene que simular a todas
las MT, pero además sabemos que es decidible porque eventualmente para, una vez
pasado el tiempo o alcanzado el número de pasos.


### La Jerarquía de Chomsky

| Lenguaje | Gramática | Máquina | Ejemplo  |
|:--------:|:---------:|:-------:|:--------:|
| NCNR | -- | -- | {{< katex >}}REGULAR, \overline{REGULAR} {{< /katex >}}
| co-RE/R |  {{< katex >}} \alpha \rightarrow \beta {{< /katex >}} | MT,ADP,MTₖ,... | A<sub>̅M̅T</sub>, L<sub>̅h</sub>/L <sub>̅U̅t</sub> |
| RE/R |  {{< katex >}} \alpha \rightarrow \beta {{< /katex >}} | MT,ADP,MTₖ,... | A<sub>MT</sub>, L<sub>h</sub>/L<sub>Ut</sub> |
| Lenguaje Dependientes del Contexto | {{< katex >}} \gamma A \beta \rightarrow \gamma \alpha \beta {{< /katex >}} | Autómata Lineal con Frontera | {{< katex >}} a^nb^nc^n {{< /katex >}} |
| Lenguaje Independiente del Contexto | {{< katex >}} A \rightarrow \alpha {{< /katex >}} | Autómata de Pila | {{< katex >}} a^nb^n {{< /katex >}} |
| Lenguaje Regular | {{< katex >}} A \rightarrow bC | d {{< /katex >}} | AF, AFND y AFND-ε  | {{< katex >}} a^n {{< /katex >}} |

