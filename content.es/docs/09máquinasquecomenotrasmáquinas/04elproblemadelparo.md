---
weight: 94
title: "El problema del paro"
description: "Se presenta el problema del paro"
---

Vimos que la máquina _M<sub>mm</sub>_ puede construirse basándose en el
comportamiento de la MTU y podemos visualizar algún aspecto del comportamiento
de la máquinas de Turing, en este caso se puden separar en dos grupos todas la
MT, las que se aceptan a si mismas y las que no. Por supuesto este
comportamiento no es tan relevante, y no hay muchas más formas en que podamos
alambrar la _M<sub>U</sub>_ para saber propiedades basadas en ellas. 

### El problema del paro

Una propiedad que sería interesante averiguar para las MT es si dada una
descripción _<m>_ de una MT _M_ y una cadena _w_ la máquina _M_ en algún momento
para su ejecución y determina si acepta o no a la cadena. A este problema se le
conoce como el problema del paro, y oor supuesto queremos
construir una máquina _M<sub>h</sub>_ que averigüe específicamente este
comportamiento de otras MTi (_h_ por _halt_ que en inglés significa paro).

Para el problema del paro no podemos usar directamente MTU porque si se queda la
máquina representada _m_ no va a parar, entonces MTU tampoco lo hará y no
podremos saber si esta máquina sigue trabajando porque necesita más tiempo o
porque está trabada, por eso la urgencia de diseñar a _M<sub>h</sub>_.
 

Usando nuestro esquema de cajas negras la máquina luciría de esta forma:

<center>
{{< figure src="../mh.svg" title="Diseño de MT para el problema del paro" >}}
</center>

### Preguntas sobre M<sub>h</sub>

Supongamos que la MT que determina si otra MT para para una _w_ especifica
existe, entonce la siguiente pregunta es cómo luce dicha máquina. Un aspecto
importante es que nos gustaría que dicha máquina fuera decidible, es decir para
toda combinación _m_ y _w_ la máquina pudiera decir si para o no para, este es
tipo de máquina que sería útil para determinar la propiedad de paro de todas la
MT.

Antes de ponernos a diseñarla, tenemos que ver algunas consecuencias de que
_M<sub>h</sub>_ sea decidible. Una consecuencia es que si _M<sub>h</sub>_ es
decidirle es posible construir una máquina _Z_ basada en _M<sub>h</sub>_ que es
evidentemente no decidible de la siguiente forma:

<center>
{{< figure src="../z.svg" title="MT Z no decidible basada en la MT del paro" >}}
</center>

Como se puede apreciar _Z_ sólo utiliza lógica para reacomodar las entradas en
_M<sub>h</sub>_ y dada la salida de la máquina decidible, decide ciclarse o no
no, muy parecido a como definimos la máquina _M<sub>mm</sub>_ que identifica
máquinas que se aceptan a si mismas. 

La secuencia lógica hasta este punto es si _M<sub>h</sub>_ es decidible entonces
Z existe.

### El problema con _Z_

El problema con _Z_ es determinar si es una máquina que se acepta a si misma o
no, es decir si pertenece a _L<sub>mm</sub>_ o no. Pareciera trivial, pero
habíamos establecido que este lenguaje existe y sólo existen dos opciones. 

|  | _Z_  pertenece a L<sub>mm</sub> | _Z_ no  pertenece a L<sub>mm</sub>  |
|:-|:-:|:-:|
| Comportamiento de _Z_ | se cicla consigo mismo | para consigo mismo |
| Decisión de _M<sub>h</sub>_ | Falso     | Verdadero |
| Decisión de _M<sub>Z</sub>_ | Verdadero | Se cicla  |
| Interpretación | Paro consigo mismo | Se cicló consigo mismo |

En la tabla podemos observar que la condición de entrada es diferente a la de
salida. Cuando suponemos que _M<sub>Z</sub>_ se cicla consigo mismo resulta que
para, y cuando suponemos que se para se cicla. Estamos ante una paradoja. 


### Dónde está el error

Hay dos puntos de error:

* Tratar de asignar _<M<sub>z</sub>>_ como parte del lenguaje _L<sub>mm</sub>_
* Definir _M<sub>h</sub>_ como decidible

En el primer caso no hay nada contradictorio, la decisión de si acepta o no a si
misma depende de la máquina _Z_ no depende de ningún aspecto, entonces si _Z_
existe debería estar o no. El hecho de que no podamos determinar apunta un
problema más intrinsico con _Z_. En este caso suponerlo decidible.
