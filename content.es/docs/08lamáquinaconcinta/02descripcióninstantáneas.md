---
weight: 82
title: "Descripciones instantáneas"
description: "Se presenta el concepto de descripciones instantáneas"
---

### Descripción instantánea (DI (DI))

Un aspecto interesante de las MT es que es posible representar la configuración
en la que se encuentra la máquina con la cinta, lo único que no está
presente en la cinta es el estado _q_ de la MT que acaba de producir ese estado
y que parte de la cadena se está analizando. Para lograr esto, se produce una
representación de la cinta en dónde la posición que está por ser analizada por
la MT se marca con el estado _q_.

Una descripción instantánea luce de la siguiente forma: 

{{< katex display >}}
X_1\ldots q X_i \ldots X_m
{{< /katex >}}

En dónde {{< katex >}}X_1 \ldots X_m{{< /katex >}} es una cadena compuesta por
símbolos del alfabeto de la cinta, y cuyo símbolo {{< katex >}}X_i{{< /katex >}}
es el siguiente a ser procesado por la MT y desde el estado _q_.


### DI para moverse a la derecha

Si una transición de la MT se mueve hacia la derecha como la siguiente genera la
siguiente descripción instantánea:

{{< katex display >}}
\delta(q,X_i)=(p,Y_i,R)
{{< /katex >}}

{{< katex display >}}
X_1\ldots q X_i \ldots X_m \vdash X_1\ldots  Y_ipX_{i+1} \ldots X_m 
{{< /katex >}}

Solo existe un caso especial cuando se está analizando el último símbolo de la
cadena en la cinta.

{{< katex display >}}
X_1\ldots  qX_m \vdash X_1 \ldots X_mqB 
{{< /katex >}}

### DI para moverse a la izquierda

Si una transición de la MT se mueve hacia la izquierda como la siguiente genera la
siguiente descripción instantánea:

{{< katex display >}}
\delta(q,X_i)=(p,Y_i,L)
{{< /katex >}}

{{< katex display >}}
X_1\ldots q X_i \ldots X_m \vdash X_1\ldots  pX_{i-1}Y_i \ldots X_m 
{{< /katex >}}

Solo existe un caso especial cuando se está analizando el primer símbolo de la
cadena en la cinta.

{{< katex display >}}
qX_1\ldots  X_m \vdash pBY_1 \ldots X_m 
{{< /katex >}}


#### Ejemplo de descripción instantaneas

La siguiente es un ejemplo de una secuencia de descripción isntantánea para la
cadena _aaabbb_

{{< katex display >}}
\begin{array}{ll}
  q_0a aabbb 
& \vdash  X q_1a abbb \\
& \vdash  Xa q_1a bbb  \\
& \vdash  Xaa q_1b bb  \\
& \vdash  Xa q_2a Ybb  \\
& \vdash  X q_2a aYbb  \\
& \vdash   q_2X aaYbb  \\
& \vdash  X q_0a aYbb  \\
& \vdash  XX q_1a Ybb  \\
& \vdash  XXa q_1Y bb  \\
& \vdash  XXaY q_1b b  \\
& \vdash  XXa q_2Y Yb  \\
& \vdash  XX q_2a YYb  \\
& \vdash  X q_2X aYYb  \\
& \vdash  XX q_0a YYb  \\
& \vdash  XXX q_1Y Yb  \\
& \vdash  XXXY q_1Y b  \\
& \vdash  XXXYY q_1b \\
& \vdash  XXXY q_2Y Y  \\
& \vdash  XXX q_2Y YY  \\
& \vdash  XX q_2X YYY  \\
& \vdash  XXX q_0Y YY  \\
& \vdash  XXXY q_3Y Y  \\
& \vdash  XXXYY q_3Y \\
& \vdash  XXXYYY q_3 B \\
& \vdash  XXXYY q_4Y B  \\
\end{array}
{{< /katex >}}
