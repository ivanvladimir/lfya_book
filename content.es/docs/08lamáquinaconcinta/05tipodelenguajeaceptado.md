---
weight: 85
title: "Tipo de lenguaje aceptado"
description: "Se reflexiona sobre el tipo de lenguaje aceptado por un MT"
---

Hasta este momento hemos visto el formalismo de las MT, sus equivalencias y el
lenguaje que acepta una MT esecífica. Sin embargo, no hemos visto el tipo de
lenguaje que acepta, al tipo de lenguaje que acepta una MT se le conoce como
<mark>recursivo</mark>. Por el momento lo que sabemos es este tipo de lenguajes
contiene a los lenguajes sensibles dependientes del contexto, pero contiene
lenguajes que no lo son. 


| Lenguaje | Gramática | Máquina | Ejemplo  |
|:--------:|:---------:|:-------:|:--------:|
| Recursivos |  {{< katex >}} \alpha \rightarrow \beta {{< /katex >}} | MT, ADP, MTₖ, ... | ?? |
| Lenguaje Dependientes del Contexto | {{< katex >}} \gamma A \beta \rightarrow \gamma \alpha \beta {{< /katex >}} | Autómata Lineal con Frontera | {{< katex >}} a^nb^nc^n {{< /katex >}} |
| Lenguaje Independiente del Contexto | {{< katex >}} A \rightarrow \alpha {{< /katex >}} | Autómata de Pila | {{< katex >}} a^nb^n {{< /katex >}} |
| Lenguaje Regular | {{< katex >}} A \rightarrow bC | d {{< /katex >}} | AF, AFND y AFND-ε  | {{< katex >}} a^n {{< /katex >}} |

