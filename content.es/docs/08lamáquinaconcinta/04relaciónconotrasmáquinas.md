---
weight: 84
title: "Relación con otras máquinas"
description: "Se expone la relación de las MT con otras máquinas"
---

Las MT son equivalentes a varias otras máquinas y a otros formalismos. 

### MT y ADP

Un Autóma de Doble Pila es equivalente a un MT, ya que  un ADP puede simular a
una MT y una MT puede simular a un ADP.

* **ADP → MT** Un ADP puede simular la cinta con sus dos pilas, la idea
    principal es simular la posición en la cinta de MT con respecto a las dos
    pilas, lo que está a la derecha del cabezal de la MT en una pila y lo que
    esté a la izquierda en otra pila. Inicialmente una cadena _w_ a ser
    analizada por un APD está localizada en un lugar diferente que a los
    elementos de la memoria (dos pilas), así que lo primero que tendría que
    hacer este simulador es poner en la pila derecha toda la cadena, a partir de
    ahí se usa no-determinismo para programar los movimientos de la MT.
*  **MT → ADP** Para simular un ADP en una MT, basta con definir un lado de la
    cinta como una pila, y el otro lado de la cinta para la segunda pila. Ojo,
    hay que considerar que en la cinta contendrá la cadena _w_, por lo que estas
    pilas simuladas tendrán que evitar tocar la cadena.

Dado que estas simulaciones son posibles podemos considerar que ambas máquinas
son equivalentes. Todo lo que concluyamos para MT es cierto para ADP. 


### MTₖ Múltiples cintas

Cuando estábamos presentando AP fue posible aumentar el poder de la máquina
agregando una nueva pila, pero también cabe recordar que agregar otra tercera
pila no aumenta el poder de la de dos pilas. Una pregunta que surge, qué pasa si
aumento una nueva cinta al diseño de la MT. A esta máquina se le conoce como una
Máquina de Turing con _k_ cintas, dependiendo de cuantas se agrega. Sin embargo
una MT son equivalentes MTₖ.

Si pensamos por un segundo suena lógico del hecho que tres pilas son tan
poderosas como dos, y como vimos era posible simular en una cinta dos pilas; si
agregamos una cinta, es como si agregáramos dos pilas a lo que es equivalente a
un ADP, por lo tanto tendríamos cuatro pilas que sabemos que son tan poderosas
como dos pilas. 

Por lo tanto todo lo que concluyamos para MT es cierto para MTₖ.

### MT semi-finita

¿Qué pasa si en lugar de aumentar cinta, disminuimos la cinta, cortándola a la
mitad? por ejemplo, dejando sólo el lado derecho. Resulta en una MT semifinita
que también es equivalente a la MT, esta máquina tendrá que tener cuidado con
ese límite y tendrá que programarse para siempre poner datos a la derecha.

Por lo tanto todo lo que concluyamos para MT es cierto para MT semi-finíta.

### Una computadora

¿Qué hay de computadoras físicas? También son equivalentes. Al principio del
curso se hizo un esfuerzo por reducir su complejidad para que fueran una caja
negra. Sin embargo, podríamos considerar que una computadora es una conjunto de
cintas independientes, tanto para dispositivos de entrada como de salida, y la
MT tendría que controlar el comportamiento de todas estas cintas. Por lo tanto,
una computadora es equivalente a una MT.

Por lo tanto todo lo que concluyamos para MT es cierto para nuestras
computadoras.

### Gramáticas de Frase

<div class="definition">
Una <mark>Gramática de Frase</mark> es una tupla {{< katex >}}(V,\Sigma,P,S){{< /katex >}} donde:


* {{< katex >}}V{{< /katex >}} es un alfabeto de <mark>símbolos no
    terminales</mark> o símbolos auxiliares
* {{< katex >}}\Sigma{{< /katex >}} es un alfabeto de <mark>símbolos
    terminales</mark>, que conforman nuestras cadenas
* {{< katex >}}P{{< /katex >}} es un <mark>conjunto de reglas</mark> que con la forma
    {{< katex >}}\alpha  \rightarrow  \beta{{< /katex >}} donde 
    {{< katex >}}\alpha \in \left(\Sigma \cup V \right)^*, \beta \in \left(\Sigma \cup V \right)^*,{{< /katex >}}  
* {{< katex >}}S \in V{{< /katex >}} lo denominamos <mark>símbolo inicial</mark>
</div>

Una gramática de frase es equivalente a una MT. Como vemos la gramática de frase
representa el proceso de re-escritura con menos restricciones posibles en sus
reglas, ya que las reglas pueden re-escribir cualquier patrón a cualquier
patrón, sin importar el contexto, la longitud o si es no terminal.


Por lo tanto todo lo que concluyamos para MT es cierto para las gramáticas de
frase.

### Otras equivalencias

Adicionalmente se ha probado que las MT son equivalentes a los siguientes
formalismos

* [Cálculo lambda](https://es.wikipedia.org/wiki/C%C3%A1lculo_lambda)
* [Funciones recursivas](https://es.wikipedia.org/wiki/Funci%C3%B3n_recursiva)
* [Algoritmos de Markov](https://en.wikipedia.org/wiki/Markov_algorithm)
* [Autómata de cola](https://en.wikipedia.org/wiki/Queue_automaton)

Por lo tanto todo lo que concluyamos para MT es cierto para cálculo lambda,
funciones recursivas, algoritmos de Markov y Autómata de cola. 
