---
weight: 42
title: "Ambigüedad"
description: "Se presenta el concepto de ambigüedad"
---

En [sección anterior]({{< ref "05arbolesdederivación#ejemplo-árbol-de-derivación-para-la-cadena-_aaabbb_" >}})
vimos algunas particularidades sobre los árboles de derivación:

1. La gramática {{< katex >}} S \rightarrow aSb | \varepsilon  {{< /katex >}}
   produce sólo una derivación y un solo árbol de derivación
2. La gramática para las Expresiones Regulares presentada en esa sección produce
   múltiples derivaciones, para algunas de estas derivaciones tiene mismo árbol de
   derivación, pero en general tienen múltiples árboles de derivación.

Hasta este momento tenemos:

* Una cadena puede tener múltiples derivaciones
* Varías derivaciones pueden corresponder a un árboles de derivación, por lo
    tanto una cadena aunque tenga múltiples derivaciones puede tener un sólo
    árbol de derivación.
* Sino se cumple lo anterior, una cadena puede tiene múltiples árboles de derivación

A la propiedad de que una cadena estar asociada a múltiples árboles se le conoce
como <mark>ambigüedad</mark>. Decimos quela cadena es <mark>ambigua</mark>. 

### Ambigüedad para los humanos

La ambigüedad es muy importante para las humanos, en lenguaje natural es común
encontrar para una misma oración (_cadena_) múltiples sentidos (_árboles de
derivación_):

* Veo al gato con el telescopio

  ¿Quién tiene el telescopio, yo o el gato?

Para este caso estos son los dos árboles:

<center>
{{< figure
src="../veo_gato_telescopio2.svg" title="Interpretación el gato tiene el telescopio" >}}
</center>

<center>
{{< figure
src="../veo_gato_telescopio1.svg" title="Interpretación yo tengo el telescopio" >}}
</center>


De hecho, la ambigüedad alguna veces crea una reacción en nosotros, risa:

* ¿Cómo te llamas?</br>
  María de los Ángeles</br>
  {{< details title="Respuesta chiste" open=false >}}
  Yo soy Carlos de Nueva York
  {{< /details >}}


* Oye, pues mi hijo en su nuevo trabajo se siente como pez en el agua </br>
  ¿Qué hace? </br>
  {{< details title="Respuesta chiste" open=false >}}
  Nada
  {{< /details >}}

De alguna forma la _ambigüedad_ son las _cosquillas del cerebro 🤯_.


## Gramáticas ambiguas

Hemos establecido que una cadena puede ser ambigua, y en particular se debe por
la gramática que la genera; entonces de alguna forma la gramática es quien
impone esa propiedad sobre la cadena. Con esto en mente, vamos a definir que una
gramática es ambigua si genera una cadena ambigua, con una sola cadena que se
genere que tenga dos árboles de derivación a la gramática se le considera
ambigua. Tomando en cuenta esta definición ¿la gramática [vista para las cadenas de la
forma]({{< ref "../04abroparéntesisabroparéntesiscierroparéntesis/03gramáticaslibresdecontexto/#ejemplo-de-glc" >}}) {{< katex >}} a^nb^n {{< /katex >}} es ambigua?
{{< details title="Respuesta" open=false >}}
No, ninguna cadena generada genera dos árboles de derivación.
{{< /details >}}

¿Y [la gramática de cadenas de expresiones
regulares]({{< ref "../04abroparéntesisabroparéntesiscierroparéntesis/04ejemplogramática/" >}})?
{{< details title="Respuesta" open=false >}}
Sí, al menos la cadena {{< katex >}}(a^*ba^*ba*^a)^*{{< /katex >}} tiene
múltiples árboles de derivación.
{{< /details >}}


