---
weight: 44
title: "Lenguaje ambiguo"
description: "Ejemplo de un lenguaje ambiguo"
---

Hasta ahora hemos visto a la ambigüedad como una propiedad de las gramáticas,
pero considere los siguientes lenguajes:

1. El lenguaje formado por cadenas con la forma {{< katex >}} a^nb^nc^md^m {{< /katex >}} 
1. El lenguaje formado por cadenas con la forma {{< katex >}} a^nb^mc^md^n {{< /katex >}} 

La siguiente gramática genera el primer lenguaje:
{{< katex display >}} 
\begin{array}{rl}
  X \rightarrow & YZ \\
  Y \rightarrow & aYb | \varepsilon\\
  Z \rightarrow & cZd | \varepsilon\\
\end{array}
{{< /katex >}} 
Un ejemplo de cadena que pertenece al lenguaje es _aabbcd_.


La siguiente gramática genera el segundo lenguaje:
{{< katex display >}} 
\begin{array}{rl}
R \rightarrow & aRd | T\\
T \rightarrow & bTc | \varepsilon\\
\end{array}
{{< /katex >}} 
Un ejemplo de cadena que pertenece al lenguaje es _aabcdd_.

Si deseamos el lenguaje que genere ambos lenguajes, la siguiente gramática lo
genera:
{{< katex display >}} 
\begin{array}{rl}
S \rightarrow & X | R \\
X \rightarrow & YZ \\
Y \rightarrow & aYb | \varepsilon\\
Z \rightarrow & cZd | \varepsilon\\
R \rightarrow & aRd | T\\
T \rightarrow & bTc | \varepsilon\\
\end{array}
{{< /katex >}} 

Usando esta gramática produce el siguiente árbol de derivación para la cadena _aabbcd_ 

<center>
{{< figure src="../aabbcd.png" title="Árbol de derivación para la cadena aabbcd" >}}
</center>

Y el siguiente árbol de derivación para la cadena _aabcdd_ 

<center>
{{< figure src="../aabcdd.png" title="Árbol de derivación para la cadena aabcdd" >}}
</center>

Y los siguientes árboles de derivación para _aabbccdd_

<center>
{{< figure src="../aabbccdd.png" title="Árbol de derivación para la cadena aabbccdd" >}}
</center>

### No hay gramática no ambigua

Como se puede ver, dado que para la cadena _aabbccdd_ hay dos árboles de
derivación, la gramática es ambigua. Sin embargo, no hay forma de organizar una
gramática que genere cadenas de alguno de los lenguajes originales sin que sea
ambigua. En este caso la propiedad de ambigüedad viene del lenguaje, el que es
ambiguo es el lenguaje y no la gramática.



