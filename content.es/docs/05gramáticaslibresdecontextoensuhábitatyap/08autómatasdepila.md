---
weight: 48
title: "Autómatas de pila"
description: "Se presenta a los autómatas de pila"
---

Para los Lenguajes Libres de Contextos, LLC, hemos visto que existe una
gramática que los genera pero no hemos visto la máquina que los acepta, el
Autómata de Pila será esta máquina. 


### AP

<div class="definition">
Un <mark>Autómata de Pila (AP)</mark> es una tupla {{< katex
>}}(Q,\Sigma,\Gamma,q_0,Z_0,A, \delta ){{< /katex >}} donde:

* {{< katex >}}Q{{< /katex >}} es un conjunto de estados finitos
* {{< katex >}}\Sigma{{< /katex >}} es un alfabeto de símbolos terminales
* {{< katex >}}\Gamma{{< /katex >}} es un alfabeto de la pila
* {{< katex >}}q_0{{< /katex >}} es un estado que denominaremos inicial donde
    {{< katex >}}q_0 \in Q{{< /katex >}}
* {{< katex >}}Z_0{{< /katex >}} es un símbolo de la pila que denominaremos
    inicial de la pila donde
    {{< katex >}}Z_0 \in \Gamma{{< /katex >}}
* {{< katex >}}A{{< /katex >}} es un conjunto de estados que denominaremos
    finales donde {{< katex >}}A \subset Q{{< /katex >}}
* {{< katex >}}\delta{{< /katex >}} es una función de transición que cumple con:
    <mark>{{< katex >}}\delta:Q \times (\Sigma\cup \{\varepsilon\}) \times \Gamma \rightarrow Q \times \Gamma^*{{< /katex >}}</mark>
</div>

#### Ejemplo de AP

El siguiente autómata reconoce al lenguaje formado de cadenas {{< katex>}}a^nb^n{{< /katex >}}
para cuando _n>0_:

<center>
{{< figure src="../anbn.svg" title="AP para cadenas aⁿbⁿ con n>0" >}}
</center>

Al igual que con los AF, al comenzar el análisis se comienza en el estado
inicial y de ahí se tienen que poner atención a tres aspectos, el estado en el
que se está, el símbolo en la cadena que está siendo analizado y el símbolo que
se encuentra hasta arriba de la pila. El siguiente análisis muestra el proceso
para la cadena _aaabbb_:


<center>
{{< figure src="../anbn.gif" title="Análisis para la cadena aaabbb" >}}
</center>


