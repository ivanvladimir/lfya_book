---
weight: 5
title: "Gramáticas libres de contexto en su hábitat… y AP"
bookCollapseSection: true
---

{{< section >}}

## Enlaces para explorar

* <i>[Reverse Polish Notation and The Stack -
    Computerphile](https://www.youtube.com/watch?v=7ha78yWRDlE)</i>. (2014, 21 mar)
* <i>[The Stack: Last in, First out](https://www.youtube.com/watch?v=IWQ74f2ot7E)</i>. (2017, 1 may)
* <i>[Pushdown Automata (Introduction)](https://www.youtube.com/watch?v=4ejIAmp_Atw)</i>. (2017, 22 jul)
* <i>[Autómata a pila: definición formal](https://www.youtube.com/watch?v=2qn_Fhmxqd4)</i>. (2017, 22 jun ) [Video]
* <i>[Autómata con pila](https://es.wikipedia.org/wiki/Aut%C3%B3mata_con_pila)</i>. (s/f) [Wikipedia]
* <i>[Gramática regular](https://es.wikipedia.org/wiki/Gram%C3%A1tica_regular)</i>. (s/f) [Wikipedia]
* <i>[Ambigüedad](https://es.wikipedia.org/wiki/Ambig%C3%BCedad)</i>. (s/f) [Wikipedia]
* <i>[Ambigüedad](https://es.wikiquote.org/wiki/Ambig%C3%BCedad)</i>. (s/f) [Wikiquote]
* <i>[Category:Ambiguity](https://commons.wikimedia.org/wiki/Category:Ambiguity)</i>. (s/f) [Wikimedia]
 

## Lecturas recomendadas

* Bar-Hillel, Y., Perles, M., & Shamir, E. (1961). On formal properties of
    simple phrase structure grammars. _Sprachtypologie und
    Universalienforschung_, 14, 143-172.
    [[Liga]](https://www.proquest.com/openview/fb41296047fb7453dcb1de182b4aa0b6/1?pq-origsite=gscholar&cbl=1817266)
* Hopcroft, J. E., Motwani, R., Ullman, J. D. Introducción a la teoría de
    autómatas, lenguajes y computación 2a. edición Madrid Pearson Education,
    2002. **Capítulo 4, 5 y 6**
    [[BiDi/UNAM](http://pbidi.unam.mx:8080/login?url=http://search.ebscohost.com/login.aspx?direct=true&db=cat02025a&AN=lib.MX001001976666&lang=es&site=eds-live)]
* Carrasco Jiménez, R. C.; Rubio, J. C.; Zubizarreta, M. L. F. Capítulo 1:
    Lenguajes y computadores. Teoría de Lenguajes, Gramáticas y Autómatas Para
    Informáticos, [s. l.], p. 11–22, 2000. **Capítulo 6 y 7**
    [[BiDi/UNAM](http://pbidi.unam.mx:8080/login?url=https://search.ebscohost.com/login.aspx?direct=true&db=ufh&jid=87PC&lang=es&site=eds-live)]
    [[Liga](https://www.digitaliapublishing.com/a/812/teoria-de-lenguajes--gramaticas-y-automatas-para-informaticos)]
* Giró J, Vazquez J, Meloni B, Constable L. Lenguajes Formales y Teoría de
    Autómatas. Alfaomega. **Capítulo 5**
    [[BiDi/UNAM](http://pbidi.unam.mx:8080/login?url=http://search.ebscohost.com/login.aspx?direct=true&db=cat02025a&AN=lib.MX001001966916&lang=es&site=eds-live)]
    [[Liga](https://libroweb.alfaomega.com.mx/book/lenguajes_formales_teoria_automatas)]
* Hernández Rodríguez LA, Jaramillo Valbuena S, Cardona Torres SA. Practique
    La Teoría de Autómatas y Lenguajes Formales. Ediciones Elizcom; 2010.
    **Capítulo 5**
    [[BiDi/UNAM](http://pbidi.unam.mx:8080/login?url=http://search.ebscohost.com/login.aspx?direct=true&db=cat02025a&AN=lib.MX001001974225&lang=es&site=eds-live)]
    [[Liga](https://www.proquest.com/docview/2135020663/D0AE11F098D846F9PQ/1)]
* Cantú Treviño, T. G., & Mendoza García, M. G. (2015). Teoría de autómatas :
    un enfoque práctico (Primera edición). Pearson Educación de México.
    **Capítulo 6**
    [[BiDi/UNAM](http://pbidi.unam.mx:8080/login?url=https://search.ebscohost.com/login.aspx?direct=true&db=cat02025a&AN=lib.MX001001976665&lang=es&site=eds-live)]
* Brena, R. (2013). Autómatas y lenguajes. McGraw-Hill Interamericana.
    **Capítulo 5**
  [[BiDi/UNAM](http://pbidi.unam.mx:8080/login?url=https://search.ebscohost.com/login.aspx?direct=true&db=cat02025a&AN=lib.MX001001698825&lang=es&site=eds-live)]
* Crespi Reghizzi S, Breveglieri L, Morzenti A. Formal Languages and
    Compilation. Second edition. Springer; 2013. **Chapter 4**
    [[BiDi/UNAM](http://pbidi.unam.mx:8080/login?url=http://search.ebscohost.com/login.aspx?direct=true&db=cat02025a&AN=lib.MX001001645742&lang=es&site=eds-live)]
    [[Liga](https://www.springer.com/gp/book/9783030048785)]


