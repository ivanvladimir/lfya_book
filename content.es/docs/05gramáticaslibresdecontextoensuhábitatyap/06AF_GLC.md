---
weight: 46
title: "AF → GLC "
description: "Se presenta como se transforma un AF a una GLC"
---

Es posible transformar un AF a una GLC, recordemos que las GLC pueden generar a
los lenguajes regulares, por lo tanto suena posible que un AF que acepta a un
Lenguaje Regular pueda trasformarse en un AF.

## Transformación

1. Cambiar los estados por símbolos auxiliares que conformaran _V_, un símbolo
   auxiliar por estado _q_ del AF.
2. Por cada transición crear una regla de producción con el siguiente formato:

{{< katex display >}}
A_{origen} \rightarrow a_{simbolo} B_{destino} 
{{< /katex >}}

Donde _A_ representa a un estado origen y _B_ un estado destino a través del
símbolo _a_.

3. Agregar una transición con la siguiente forma por cada transición del AF
   original que termine en un estado final.

{{< katex display >}}
A_{origen} \rightarrow a_{simbolo}
{{< /katex >}}


### Ejemplo 

Considere el siguiente AF:
<center>
{{< figure src="../../02lamáquinasinmemoria/bes_pares.svg" title="Número par de bes" >}}
</center>

1. Proponer símbolos auxiliares
{{< katex display >}}
V = \{A_{q_0}, B_{q_1}\}
{{< /katex >}}

1. Agregar reglas de producción por transición
{{< katex display >}}
\begin{array}{rl}
  A \rightarrow & aA \\
  A \rightarrow & aB \\
  B \rightarrow & aB \\
  B \rightarrow & bA \\
\end{array}
{{< /katex >}}

1. Agregar reglas de producción por transición que llega a estado final
{{< katex display >}}
\begin{array}{rl}
  A \rightarrow & a \\
  B \rightarrow & b \\
\end{array}
{{< /katex >}}

Nuestra gramática completa queda:
{{< katex display >}}
\begin{array}{rl}
  A \rightarrow & aA \\
  A \rightarrow & aB \\
  B \rightarrow & aB \\
  B \rightarrow & bA \\
  A \rightarrow & a \\
  B \rightarrow & b \\
\end{array}
{{< /katex >}}

La siguiente es la derivación para la cadena _aabbabaaaaba_:

{{< katex display >}}
\begin{array}{rl}
A \Rightarrow & aA\\
  \Rightarrow & aaA\\
  \Rightarrow & aabB\\
  \Rightarrow & aabbA\\
  \Rightarrow & aabbaA\\
  \Rightarrow & aabbabB\\
  \Rightarrow & aabbabaB\\
  \Rightarrow & aabbabaaB\\
  \Rightarrow & aabbabaaaB\\
  \Rightarrow & aabbabaaaaB\\
  \Rightarrow &  aabbabaaaab\\
\end{array}
{{< /katex >}}

Dada la forma de las reglas es posible ver que siempre hay un sólo símbolo no
terminal en lado derecho de la derivación, esto origina que sólo haya una
derivación posible y además un árbol de derivación (que se muestra a
continuación), es decir este tipo de gramáticas no son ambiguas.

<center>
{{< figure src="../gr.png" title="Árbol de derivación para la cadena aabbabaaaab" >}}
</center>


