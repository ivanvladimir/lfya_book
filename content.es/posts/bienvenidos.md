---
author: "Ivan Meza"
date: 2020-08-16
linktitle: ¡Bienvenidos!
menu:
  main:
      parent: tutorials
title: Bienvenidos al curso
weight: 10
---

Este es el blog post para el libro de Lenguajes Formales y Autómatas que acompaña a mi curso del mismo nombre que importo en la [Facultade de Ingeniería](http://www.ingenieria.unam.mx) de la [UNAM](http://www.unam.mx).

Para noticias sobre el desarrollo sigue al sitio.
