---
author: "Ivan Meza"
date: 2021-09-12
linktitle: ¡Notas en PDF!
menu:
  main:
      parent: tutorials
title: ¡Notas en PDF!
weight: 10
---

Ahora las notas se encuentras en formato [pdf](https://drive.google.com/file/d/1rcw4wG-52bxkZxFojg6dqa_vLMnAa5yj/view?usp=sharing)
